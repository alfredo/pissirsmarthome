#!/bin/bash

javac -cp .:./org.eclipse.paho.client.mqttv3_1.2.5.jar:./org.json-1.0.0.v201011060100.jar:./bcpkix-jdk13-167.jar:./bcprov-jdk13-167.jar *.java
echo 'cloudapp compiled'

java -classpath .:./org.eclipse.paho.client.mqttv3_1.2.5.jar:./org.json-1.0.0.v201011060100.jar:./bcpkix-jdk13-167.jar:./bcprov-jdk13-167.jar  RESTHttpServer CONF/conf.xml
