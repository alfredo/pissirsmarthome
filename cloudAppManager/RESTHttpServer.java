import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.security.KeyStore;
import java.util.concurrent.Executors;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.TrustManagerFactory;

import com.sun.net.httpserver.HttpsConfigurator;
import com.sun.net.httpserver.HttpsParameters;
import com.sun.net.httpserver.HttpsServer;
import com.sun.net.httpserver.HttpServer;

public class RESTHttpServer {

	static public int port=8080;
	static public Conf conf;
	static public Publisher mqtt_client;

	public static void main(String[] args) throws IOException {
		if(args.length>=1)
		try {
		    conf = new Conf(args);
		}
		catch (Exception e) {
			e.printStackTrace();
			return;
		}
		port = Integer.parseInt(conf.get("port"));
 		mqtt_client = new Publisher(conf.protocol+"://"+conf.broker+":"+conf.get("portMqtt"));
		mqtt_client.start();
        HttpServer server = HttpServer.create(new InetSocketAddress(port),0);


        //API del server
        server.createContext("/apirest", new HtmlPage(conf.confdir));
        server.createContext("/install", new Install(mqtt_client));
        server.createContext("/start", new Start(mqtt_client));
        server.createContext("/stop", new Stop(mqtt_client));
        server.createContext("/delete", new Delete(mqtt_client));
        // ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor)Executors.newFixedThreadPool(5);
        server.setExecutor(Executors.newCachedThreadPool());
        server.start();
        System.out.println("cloudapp running on http://localhost:"+port);
	}
}
