package code;

import java.io.IOException;
import java.net.MalformedURLException;
import java.io.*;
import java.util.HashMap;
import java.util.Arrays;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class KeyCloak extends HashMap<String,String> {

	public String extraBody() {
		return get("extrabodyTemplate").replace("$CLIENTID",get("client-id"));
	}

	public String authServer() {
		String val = get("auth-server-url");
		int i = val.length()-2;
		while(i>0 && val.charAt(i)!='/') i--;
		return val.substring(i+1,val.length()-1);
	}

	public String codeRQ () {
		String val = get("coderq").replace("$CLIENTID",get("client-id"));
		return val.replace("$REALM", get("realm"));
	}


	public String tokenRQ () {
		String val = get("tokenrq").replace("$CLIENTID",get("client-id"));
		return val.replace("$REALM", get("realm"));
	}
	
	public String realm() {
		return get("realm");
	}
	
	
	public String redirectUri() {
		return get("redirect-uri");
	}

	public void print() {
		System.out.println(Arrays.asList(this));
	}

	public KeyCloak(String keycloakpath, String params) throws MalformedURLException, IOException {
		BufferedReader data;
		String json = "";
		String line;
		data = new BufferedReader(new FileReader(keycloakpath));
		while((line = data.readLine())!=null) json += line;
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(json);
		if (element.isJsonObject()) {
			JsonObject keycloak = element.getAsJsonObject();
			put("realm", keycloak.get("realm").getAsString());
			put("auth-server-url", keycloak.get("auth-server-url").getAsString());
			put("ssl-required", keycloak.get("ssl-required").getAsString());
			put("resouce", keycloak.get("resource").getAsString());
			put("client-id", keycloak.get("resource").getAsString());
			put("public-client", keycloak.get("public-client").getAsString());
			put("confidential-port", keycloak.get("confidential-port").getAsString());
		}
		data.close();
		BufferedReader data2 = new BufferedReader(new FileReader(params));
		json = "";
		while((line = data2.readLine())!=null) json += line;
		element = parser.parse(json);
		if (element.isJsonObject()) {
			JsonObject parameters = element.getAsJsonObject();
			put("coderq", parameters.get("coderq").getAsString());
			put("tokenrq", parameters.get("tokenrq").getAsString());
			put("extrabodyTemplate", parameters.get("extrabody").getAsString());
			put("redirect-uri", parameters.get("redirect_uri").getAsString());
		}
		data2.close();

	}
}


