package code;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.security.KeyStore;
import java.util.concurrent.Executors;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.TrustManagerFactory;

import com.sun.net.httpserver.HttpsConfigurator;
import com.sun.net.httpserver.HttpsParameters;
import com.sun.net.httpserver.HttpsServer;

public class Server {

//    private static int port = 443;
    private static int port = 3000;
	public final static String CLIENT_PATH = "../public";

    public static void main(String[] args) throws IOException {
        KeyCloak kcs = new KeyCloak(CLIENT_PATH + "/keycloak.json", CLIENT_PATH + "/params.json");
        HttpsServer server = HttpsServer.create(new InetSocketAddress(port), 0);//port gets set here
        // HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);

        // initialise the HTTPS server
        try {
            SSLContext sslContext = SSLContext.getInstance("TLS");

            // initialise the keystore
            char[] password = "miapasswd".toCharArray();
            KeyStore ks = KeyStore.getInstance("JKS");
            FileInputStream fis = new FileInputStream("../lig.keystore");
            ks.load(fis, password);

            // setup the key manager factory
            KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
            kmf.init(ks, password);

            // setup the trust manager factory
            TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
            tmf.init(ks);

            // setup the HTTPS context and parameters
            sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
            server.setHttpsConfigurator(new HttpsConfigurator(sslContext) {
                @Override
                public void configure(HttpsParameters params) {
                    try {
                        // initialise the SSL context
                        SSLContext context = getSSLContext();
                        SSLEngine engine = context.createSSLEngine();
                        params.setNeedClientAuth(false);
                        params.setCipherSuites(engine.getEnabledCipherSuites());
                        params.setProtocols(engine.getEnabledProtocols());

                        // Set the SSL parameters
                        SSLParameters sslParameters = context.getSupportedSSLParameters();
                        params.setSSLParameters(sslParameters);

                    } catch (Exception ex) {
                        System.out.println("Failed to create HTTPS port");
                        ex.printStackTrace();
                    }
                }
            });

            //API del server
            server.createContext("/js/authentication/",new Resources());
            server.createContext("/js/templates/",new Resources());
            server.createContext("/css/",new Resources());
            server.createContext("/res/",new ImageRes());
            server.createContext("/secured/home/js/",new Resources());
            server.createContext("/secured/home/css/",new Resources());
            server.createContext("/secured/home/res/",new ImageRes());
            server.createContext("/secured/home/",new Home());
            server.createContext("/secured",new ObtainToken(kcs));
            server.createContext("/",new ObtainToken(kcs));

            server.setExecutor(Executors.newCachedThreadPool());
			server.start();
            System.out.println("webserver running on https://localhost:"+port);
        } catch (Exception e) {
        	System.out.println("Failed to create HTTPS server on port " + port + " of localhost");
            e.printStackTrace();
        }
    }

}
