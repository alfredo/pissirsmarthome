package code;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.json.JSONException;
import org.json.JSONObject;

import com.sun.net.httpserver.HttpsExchange;

public class Helper {

	private static void sendResponse(HttpsExchange exchange, byte[] bytes, int code) throws IOException{
		exchange.sendResponseHeaders(code, bytes.length);
		OutputStream os = exchange.getResponseBody();
		os.write(bytes);
		os.close();
	}

	private static void sendResponse(HttpsExchange exchange, String response, int code) throws IOException{
		sendResponse(exchange,response.getBytes(),code);
	}

	public static void sendResponseOk(String response, HttpsExchange exchange) throws IOException {
		sendResponse(exchange, response, 200);
	}

	public static void sendResponseOk(byte[] response, HttpsExchange exchange) throws IOException {
		sendResponse(exchange, response, 200);
	}

	public static void badRequest(HttpsExchange exchange) throws IOException {
		System.out.println("Errors in the request!");
		String response = "{\"message\":\"Errors in the request!\"}";
		sendResponse(exchange, response, 400);
	}

	public static void pageNotFound(HttpsExchange exchange) throws IOException {
		System.out.println("Page not found!");
		String response = "{\"message\":\"Page not found!\"}";
		sendResponse(exchange, response, 404);
	}

	public static void methodNotAllowed(HttpsExchange exchange) throws IOException {
		System.out.println("Method not allowed!");
		String response = "{\"message\":\"Method not allowed!\"}";
		sendResponse(exchange, response, 405);
	}

	public static void serverError(HttpsExchange exchange) throws IOException{
		System.out.println("Server error!");
		String response = "{\"message\":\"Server error!\"}";
		sendResponse(exchange, response, 500);
	}

	public static boolean isSameString(String a, String b){
		return a.compareToIgnoreCase(b) == 0;
	}

	public static String readBody(InputStream requestBody) {
		int req;
		StringBuffer sb = new StringBuffer();
		try {
			while((req = requestBody.read()) != -1)
				sb.append(Character.toString((char)req));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	public static boolean checkJSON(String body) {
		try {
			new JSONObject(body);
			return true;
		} catch (JSONException e) {
			return false;
		}
	}

	public static String getExtension(String file) {
        int i = file.length() - 1;
        while (i > 0 && file.charAt(i) != '.' && file.charAt(i) != '/')
            i--;
        if (file.charAt(i) == '.')
            return file.substring(i + 1);
        else
            return "";
    }

	public static String getPageContents(String fileName){
        String line;
        String page = Server.CLIENT_PATH+(fileName.startsWith("/") ? fileName : "/"+fileName);

        StringBuilder answer = new StringBuilder();
        if (Helper.getExtension(page).length() == 0)
            page += ".html";

        BufferedReader bufferedReader = null;
        try {
            FileReader fileReader = new FileReader(page);

            bufferedReader = new BufferedReader(fileReader);
            boolean isComment = false;
            while ((line = bufferedReader.readLine()) != null) {
            	line = line.trim();

            	if(line.startsWith("<!--") && line.endsWith("-->")) {
            		continue;
            	}
            	if(line.startsWith("<!--")) {
            		isComment = true;
            		continue;
            	}
            	if(line.endsWith("-->")) {
            		isComment = false;
            		continue;
            	}

            	if(!isComment && line.length()>0)
                	answer.append(line).append("\n");
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Unable to open file '" + page + "'");
            return "fail";
        } catch (IOException ex) {
            System.out.println("Error reading file '" + page + "'");
            return "fail";
        } finally {
            try{
                if(bufferedReader != null)
                    bufferedReader.close();
            } catch (IOException ex){
                System.out.println("Error closing bufferedReader");
            }
        }
        return answer.toString();
    }
}
