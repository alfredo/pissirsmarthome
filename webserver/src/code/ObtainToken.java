package code;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpsExchange;

public class ObtainToken implements HttpHandler{

	private KeyCloak kcs;

	public ObtainToken(KeyCloak kcs) {
		this.kcs = kcs;
	}

	@Override
    public void handle(HttpExchange ex) throws IOException {
        HttpsExchange exchange = (HttpsExchange) ex;
        URI requestURI = exchange.getRequestURI();
        String stringURI = requestURI.toString();
        boolean wantsRedirectPage = Helper.isSameString(stringURI,URI.create("/").toString());
        boolean wantsToken = Helper.isSameString(stringURI,URI.create("/secured").toString());

        if(!wantsRedirectPage && !wantsToken) {
            Helper.pageNotFound(exchange);
            return;
        }

        String requestMethod = exchange.getRequestMethod();
        if (!Helper.isSameString(requestMethod, "GET")) {
            Helper.methodNotAllowed(exchange);
            return;
        }
        // get the html page
        String response = null;
        if(wantsRedirectPage)
        response = Helper.getPageContents("redirect.html");
        else if(wantsToken)
        response = Helper.getPageContents("domains.html");

        if(response == null || Helper.isSameString(response, "fail")){
            Helper.serverError(exchange);
            return;
        }
        response = response.replace("$DOMAIN", kcs.authServer())
        .replace("$REALM", kcs.realm())
        .replace("$MY_REDIRECT_URI", kcs.redirectUri());

        List<String> strlist = new ArrayList<>();
        strlist.add("text/html");
        exchange.getResponseHeaders().put("content-type", strlist);
        Helper.sendResponseOk(response, exchange);
    }

}