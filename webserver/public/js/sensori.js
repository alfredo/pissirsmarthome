"use strict";

const sensoreTemplate = {
    nome: "",
    stato: false,
    utilizzo: "",
};

class Sensori {

    static sensoriBucketIn = [];
    static sensoriBucketOut = [];

    static sensoriDisponibiliIn = [];
    static sensoriDisponibiliOut = [];

    constructor() {
        Sensori.populateSensoriDisponibili();
    }
    /**
     * rimuove il sensore dalla lista dei sensori disponibili e lo aggiunge alla lista dei sensori usati; inoltre aggiorna le select
     * @param {String} nome il nome del sensore
     * @returns l'oggetto sensore
     */
    static claimSensore(nome) {
        console.log('claiming sensor ' + nome, {input:Sensori.sensoriDisponibiliIn, output:Sensori.sensoriDisponibiliOut});
        nome = nome.toUpperCase();
        if (!Sensori.checkNomeSensore(nome))
            return;
        let sensore = null;
        if (nome.startsWith("IN")) {
            const index = Sensori.sensoriDisponibiliIn.findIndex(s => s.nome === nome);
            if (index === -1) {
                // Sensori.liberaSensore(nome);
                console.log('sensore in non trovato')
                throw new Error("Errore impossibile");
            }
            sensore = Sensori.sensoriDisponibiliIn.splice(index, 1)[0];
            Sensori.sensoriBucketIn.push(sensore);
        } else if(nome.startsWith("OUT")) {
            const index = Sensori.sensoriDisponibiliOut.findIndex(s => s.nome === nome);
            if (index === -1) {
                // Sensori.liberaSensore(nome);
                console.log('sensore out non trovato')
                throw new Error("Errore impossibile");
            }
            sensore = Sensori.sensoriDisponibiliOut.splice(index, 1)[0]
            Sensori.sensoriBucketOut.push(sensore);
        } else {
	    console.log("errore nome",nome);
	    return null;
	}
        Sensori.showAvailableOptions();
        return sensore;
    }

    /**
     * aggiorna le select da cui l'utente puo' scegliere i sensori da usare
     *
     */
    static showAvailableOptions() {
        const selectIn = document.querySelectorAll('.scelta-porta-in');
        for (const select of selectIn)
            Sensori.populateOptions(select, false);
        const selectOut = document.querySelectorAll('.scelta-porta-out');
        for (const select of selectOut)
            Sensori.populateOptions(select, true);
    }
    static populateOptions(selectNode, out = false) {
        // remove all options from the selectNode
        const firstChild = selectNode.firstElementChild;
        while (selectNode.lastChild !== firstChild)
            selectNode.removeChild(selectNode.lastChild);

        // show all the available sensors as options
        const sensori = out ? Sensori.sensoriDisponibiliOut : Sensori.sensoriDisponibiliIn;
        for (const s of sensori) {
            const option = document.createElement("option");
            option.value = s.nome;
            option.innerHTML = s.nome;
            selectNode.appendChild(option);
        }
    }
    /**
     * rimette il sensore nella lista dei sensori disponibili e lo rimuove dalla lista dei sensori usati; inoltre aggiorna le select
     * @param {*} sensore il sensore che non si utilizza piu'
     */
    static liberaSensore(sensore) {
        // the sensor is removed from sensoriDisponibili but isn't yet in sensoriBucket
        console.log('freeing sensor ' +sensore?.nome);
        if (!Sensori.checkNomeSensore(sensore?.nome,true))
            return;

        if(sensore.nome.startsWith("IN")){
            Sensori.sensoriDisponibiliIn.push(sensore);
            Sensori.sensoriBucketIn.splice(Sensori.sensoriBucketIn.findIndex(s => s.nome === sensore.nome), 1);
        } else {
            Sensori.sensoriDisponibiliOut.push(sensore);
            Sensori.sensoriBucketOut.splice(Sensori.sensoriBucketOut.findIndex(s => s.nome === sensore.nome), 1);
        }
        Sensori.showAvailableOptions();
    }

    static createSensoreHTML(sensore) {
        // must set: id="sensore-in0" src="res/led-green.png" alt="sensore in0" class="" title="More info here!"
        const sensoreEl = document.createElement("li");
        sensoreEl.classList.add('d-flex', 'flex-column');
        sensoreEl.innerHTML = `
        <!--<li class="d-flex flex-column">-->
            <span class="d-flex justify-content-center">
                <img id="sensore-${sensore.nome}" src="res/led-${sensore.stato ? 'green' : 'red'}.png" alt="sensore-${sensore.nome}" title="${sensore.utilizzo}" height="70" width="70" data-bs-toggle="tooltip" data-bs-placement="bottom">
            </span>
            <p class="d-flex justify-content-center">${sensore.nome}</p>
        <!--</li>-->
        `;
        return sensoreEl;
    }
    /**
     * creates a new sensor and returns it. It can also be added to the bucket.
     * @param {*} nome
     * @param {*} stato
     * @param {*} utilizzo
     * @param {*} mostraSensore
     * @returns
     */
    static createSensoreDaValori(nome, stato, utilizzo/*, mostraSensore = false*/) {
        const sensore = Object.assign({}, sensoreTemplate);
        sensore.nome = nome.toUpperCase().trim();
        sensore.stato = stato ? stato : false;
        sensore.utilizzo = utilizzo ? utilizzo : null;
        // if (mostraSensore)
        //     Sensori.showSensore(sensore);
        return sensore;
    }
    /**
     *
     * @param {String} nome il nome del sensore
     * @returns true se il nome e' corretto ed il sensore e' disponibile
     */
    static checkNomeSensore(nome,shouldFree = false) {
        if (Sensori.sensoriBucketIn.length + Sensori.sensoriBucketOut.length >= 16 || Sensori.sensoriDisponibiliIn.length + Sensori.sensoriDisponibiliOut === 0)
            return false;
        if (nome == null || nome.length === 0)
            return false;
        if (nome.startsWith("IN")) {
            const number = nome.substring(2);
            if (number == null || isNaN(number) || isNaN(parseInt(number)) || number < 0 || number > 8)
                return false;
            if(!shouldFree){
                for (const sensore of Sensori.sensoriBucketIn)
                    if (sensore.nome === nome)
                        return false;
                return Sensori.sensoriDisponibiliIn.length > 0;
            } else {
                for (const sensore of Sensori.sensoriDisponibiliIn)
                    if (sensore.nome === nome)
                        return false;
                return Sensori.sensoriBucketIn.length > 0;
            }
        } else if (nome.startsWith("OUT")) {
            const number = nome.substring(3);
            if (number == null || isNaN(number) || isNaN(parseInt(number)) || number < 0 || number > 8)
                return false;
            if(!shouldFree){
                for (const sensore of Sensori.sensoriBucketOut)
                    if (sensore.nome === nome)
                        return false;
                return Sensori.sensoriDisponibiliOut.length > 0;
            } else {
                for (const sensore of Sensori.sensoriDisponibiliOut)
                    if (sensore.nome === nome)
                        return false;
                return Sensori.sensoriBucketOut.length > 0;
            }
        } else return false;
    }

    static populateSensoriDisponibili() {
        for (let i = 0; i < 16; i++) {
            if (i < 8) {
                Sensori.sensoriDisponibiliIn.push(Sensori.createSensoreDaValori("in" + i));
            } else {
                Sensori.sensoriDisponibiliOut.push(Sensori.createSensoreDaValori("out" + (i % 8)));
            }
        }
    }

    static initializaTooltips() {//from bootstrap, mandatory
        const tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
        const tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
            return new bootstrap.Tooltip(tooltipTriggerEl)
        });
    }
}

export default Sensori;
