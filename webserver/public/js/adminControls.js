"use strict";

import Api from "./mqtt/api.js";

class Admin {

	constructor() {
		const adminPanel = document.querySelectorAll(".admin-panel");
		for (const panel of adminPanel)
			panel.classList.remove('invisible');
		this.setupListeners();
	}

	setupListeners() {
		const adminModals = document.querySelectorAll(".admin-modal");
		for (const modal of adminModals) {
			const cancelBtn = modal.querySelector('.btn-danger');
			cancelBtn.addEventListener('click', () => {
				// get all inputs and reset them
				const inputs = modal.querySelectorAll('input');
				for (const input of inputs)
					input.value = '';
				// get all select and reset them
				const selects = modal.querySelectorAll('select');
				for (const select of selects)
					select.value = 'unselected';
			});
		}
		this.scenariPanel();
		this.antifurtoPanel();
	}

	scenariPanel() {
		const submitBtn = document.getElementById("admin-scenari-submit");
		submitBtn.addEventListener('click', () => {
			const inApprendimento = document.getElementById("admin-in-apprendimento");
			const inScenari = document.getElementById("admin-in-scenari");
			const outScenari = document.getElementById("admin-out-scenari");
			const inApprendimentoValue = inApprendimento.value;
			const inScenariValue = inScenari.value;
			const outScenariValue = outScenari.value;
			if(inApprendimentoValue === inScenariValue && inApprendimentoValue !== 'unselected'){
				alert('Non puoi selezionare lo stesso ingresso per l\'apprendimento e per gli scenari.');
				return;
			}
			let json = {};
			inApprendimentoValue !== 'unselected' ? json["learn-trigger"] = inApprendimentoValue : null;
			inScenariValue !== 'unselected' ? json["attiva-scenari"] = inScenariValue : null;
			outScenariValue !== 'unselected' ? json["luce-antifurto"] = outScenariValue : null;
			if(Object.keys(json).length === 0){
				alert('Non hai selezionato nessuna porta.');
				return;
			}
			//make the json for the antifurto
			let jsonantifurto = {};
			inScenariValue !== 'unselected' ? jsonantifurto["interruttore"] = inScenariValue : null;
			outScenariValue !== 'unselected' ? jsonantifurto["nomeOutputAntifurto"] = outScenariValue : null;
			// send the json to the server
			Api.sendScenariConfiguration(json);
			if(Object.keys(jsonantifurto).length !== 0){
				Api.sendAntifurtoConfiguration(jsonantifurto);
			}
			// close the modal
			const closeBtn = submitBtn.closest('.modal').querySelector('.btn-danger');
			closeBtn.click();
		});
	}

	antifurtoPanel() {
		const submitBtn = document.getElementById("admin-antifurto-submit");
		submitBtn.addEventListener('click', async () => {
			// prima parte di configurazione (input e 2 output per antifurto)
			const inAntifurto = document.getElementById("admin-in-antifurto");
			const outAntifurto = document.getElementById("admin-out-antifurto");
			const outAllarme = document.getElementById("admin-out-allarme");
			const inAntifurtoValue = inAntifurto.value;
			const outAntifurtoValue = outAntifurto.value;
			const outAllarmeValue = outAllarme.value;
			const inSensore = document.getElementById("admin-in-sensore");
			const deltaSensore = document.getElementById("admin-delta-sensore");
			const inSensoreValue = inSensore.value;
			const deltaSensoreValue = deltaSensore.value;

			if(outAntifurtoValue === outAllarmeValue && outAntifurtoValue !== 'unselected'){
				alert('Non puoi selezionare lo stesso uscita per lo stato e per l\'allarme.');
				return;
			}
			let json = {};
			inAntifurtoValue !== 'unselected' ? json["interruttore"] = inAntifurtoValue : null;
			outAntifurtoValue !== 'unselected' ? json["nomeOutputAntifurto"] = outAntifurtoValue : null;
			outAllarmeValue !== 'unselected' ? json["outputSuono"] = outAllarmeValue : null;
			// send the json to the server
			if(Object.keys(json).length > 0){
				Api.sendAntifurtoConfiguration(json);
				//create json for scenari
				let jsonscenari = {};
				inAntifurtoValue !== 'unselected' ? jsonscenari["attiva-scenari"] = inAntifurtoValue : null;
				outAntifurtoValue !== 'unselected' ? jsonscenari["luce-antifurto"] = outAntifurtoValue : null;
				Api.sendScenariConfiguration(jsonscenari);
			}

			// seconda parte (creazione nuovo sensore di movimento)
			if(inSensoreValue !== 'unselected' && deltaSensoreValue === ''){
				alert('Non hai inserito il valore per il sensore.');
				return;
			} else if(inSensoreValue === 'unselected' && deltaSensoreValue !== ''){
				alert('Non hai selezionato nessuna porta per il sensore.');
				return;
			}

			if(inSensoreValue !== 'unselected' && deltaSensoreValue !== ''){
				if(parseInt(deltaSensoreValue) <= 0){
					alert("Inserire un valore delta > 0");
					return;
				}
				const sensore = {
					in: inSensoreValue,
					delta: deltaSensoreValue
				};
				// send the new sensore configuration to the server
				Api.sendSensoreConfiguration(sensore);
			}

			// terza parte (soglia)
			const sogliaInput = document.getElementById('sogliaAntifurto');
			const sogliaValue = sogliaInput.value.trim();
			const soglia = parseInt(sogliaValue);
			if(!isNaN(soglia)){
				if(soglia <= 0 || soglia >=100 ){
					alert('Inserire un valore soglia tra 0 e 100');
					return;
				} else {
					const antifurto = await import('./antifurto.js');
					antifurto.default.setupSoglia(soglia);
				}
			}

			// close the modal
			const closeBtn = submitBtn.closest('.modal').querySelector('.btn-danger');
			closeBtn.click();
		});
	}

}

export default Admin;
