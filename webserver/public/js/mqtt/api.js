"use strict";

import { mqtt_tree, sendMessage } from "./mqtthat.js";

class Api {

    static antifurto = null;
    static scenari = null;
    static luci = null;

    constructor (boolArray) {
	    Api.init(boolArray);
    }

    static async init(boolArray){
        if(boolArray[0]){
            const antifurto = await import("../antifurto.js");
	    Api.antifurto = antifurto.default;
	}
        if(boolArray[1]){
            const scenari = await import("../scenari.js");
	    Api.scenari = scenari.default;
	}
        if(boolArray[2]){
            const luci = await import("../luci.js");
	    Api.luci = luci.default;
	}
    }
    static isConnected = false;

    static async onConnect(){
        // require all modules to free their resources using freeAll method
        // launch the fillTable function of each module available
        if(Api.antifurto != null){
            Api.antifurto.freeAll();
            Api.antifurto.fillTable();
        }
        if(Api.scenari != null){
            Api.scenari.freeAll();
            Api.scenari.fillTable();
        }
        if(Api.luci != null){
            Api.luci.freeAll();
            Api.luci.fillTable();
        }
    }
    // /api/luci/
    /**
     * gets the luci of the user from the server
     * @returns {*} all the lights of the user
     */
    static getLuci = () => {
        const topic = "to/all";
        const message = "{request:status}";
        sendMessage(topic, message);
    };
    static sendLuciWeb = luciInput => {
        Api.luci.luciFromMqtt(luciInput);
    }

    // static callbackGetAllLuci = null;
    static getAllLuci = () => {
        const topic = `rpc/${mqtt_tree}luci`;
        const message = "{request:description}";
        sendMessage(topic, message);
    }
    static sendDataLuciWeb = luciInput => {
        Api.luci.luciFromMqtt(luciInput);
    }

    static luceMakeNewLuci = null;
    static callbackMakeNewLuci = null;
    /**
     * adds a new luce to the server
     * @param {luceTemplate} luce the luce to add to the server
     * @returns {*} null if the light was added
     */
    static makeNewLuci = luce => {
        if (luce == null)
            throw new Error("luce must be defined");
        const luceSent = {nome: luce.luogo, stato: luce.stato, input: luce["sensore-in"], output: luce["luce-out"]};
        Api.luceMakeNewLuci = luce;

        const topic = `to/${mqtt_tree}luci/new`;
        const message = JSON.stringify(luceSent);
        sendMessage(topic, message);
    }
    static sendWebNewLuci = luce => {
        const oldLuce = Api.luceMakeNewLuci;
        const luceReceived = {luogo: luce.nome, stato: luce.stato, "sensore-in": luce.input, "luce-out": luce.output, id : oldLuce.id};
        if (oldLuce != luce) {
            console.log('le luci non corrispondono', { oldLuce, luceReceived });
        }
        Api.luci.showCreatedLight(luceReceived);
    }

    static sensoreMakeNewSensore = null;
    /**
     * invia il sensore al microservizio per aggiornarlo //FIXME YET TO BE USED
     * @param {*} sensore l'oggetto sensore che dev'essere inviato al microservizio
     */
    static makeNewSensoreLuci = sensore => {
        if (sensore == null || sensore.nome == null)
            throw new Error("sensore must be defined");
        Api.sensoreMakeNewSensore = sensore;

        const newSensore = {sensM: sensore.nome};
        const topic = `to/${mqtt_tree}luci/sensore`;
        const message = JSON.stringify(newSensore);
        sendMessage(topic, message);
    }
    static sendWebNewSensore = sensore => {
        const oldSensore = Api.sensoreMakeNewSensore;
        if(oldSensore != null && oldSensore.nome !== sensore.sensM) {
            console.log('i sensori non corrispondono', { oldSensore, sensore });
        }
        Api.luci.showCreatedSensore(sensore);//FIXME not used
    }
    static callbackLuciStatoToBeStatic = null;
    static toggleForLuciStato = null;
    static fixedCallbackLuciStato = null;
    // /api/luci/stato/
    /**
     * changes the status of a luce on the server
     * @param {luceTemplate} luce the luce to change the status of
     * @returns {*} null if the light's status was changed
     */
    static setLuciStato = (luce, toggle, callback) => {
        if (Api.fixedCallbackLuciStato == null)
            return;
        if (luce == null)
            throw new Error("luce must be defined");
        if (toggle == null)
            throw new Error("toggle must be defined");
        if (callback == null)
            throw new Error("callback must be defined");

        if(Api.callbackLuciStatoToBeStatic == null)
            Api.callbackLuciStatoToBeStatic = callback;
        Api.toggleForLuciStato = toggle;

        const topic = `from/${mqtt_tree}gpio/${luce["sensore-in"].toUpperCase()}`;
        const message = `{"event":1}`;
        // Api.outForToggleLuciStato = luce;
        sendMessage(topic, message);
    }
    static sendLuciStatoWeb = response => {
        //response == {"output":outVal,stato};
        if (response == null) {
            const toggle = Api.toggleForLuciStato;
            Api.toggleForLuciStato = null;
            Api.callbackLuciStatoToBeStatic(toggle);
        } else {
            Api.fixedCallbackLuciStato(response);
        }
    }
    // /api/scenari/
    /**
     * gets the scenari of the user from the server
     * @returns {*} all the scenarios of the user
     */
    static getScenari = () => {
        const topic = `rpc/${mqtt_tree}scenari`;
        const message = "{request:status}";
        sendMessage(topic, message);
    };
    static sendScenariWeb = scenari => {
        Api.scenari.mostraScenariServer(scenari);
    }

    static setAntifurtoINbtnFromScenari = newINbtn => {
    	console.log(newINbtn)
        if(newINbtn == null || newINbtn?.nome == null)
            throw new Error("newINbtn must be defined");

        const btn = newINbtn.nome;
        if(btn == null)
            return;
        const topic = `to/${mqtt_tree}scenari/sensoreAntifurto`;
        const message = `{"attiva-scenari":"${btn}"}`;
        sendMessage(topic, message);
    }
    // /api/scenari/attiva/
    static scenarioSetscenarioStatus = null;
    static activatingSetscenarioStatus = null;
    /**
     * activates or deactivates a scenario on the server
     * @param {scenarioTemplate} scenario the scenario to activate or deactivate
     * @returns {*} null if the scenario was activated or deactivated
     */
    static setScenarioStatus = (scenario, activating) => {
        if (scenario == null)
            throw new Error("scenario must be defined");
        if (activating == null)
            throw new Error("activating must be defined");
        Api.scenarioSetScenarioStatus = scenario;
        Api.activatingSetScenarioStatus = activating;

        const topic = `to/${mqtt_tree}scenari`;
        const message = `{"evento":${scenario.stato === false ? '0' : '1'},"nome":"${scenario.nome}"}`;
        sendMessage(topic, message);
    };
    static sendWebNewScenarioStatus = scenarioNome => {
        const scenario = Api.scenarioSetScenarioStatus;
        const activating = Api.activatingSetScenarioStatus;
        Api.scenarioSetScenarioStatus = null;
        Api.activatingSetScenarioStatus = null;
        if(scenario == null || activating == null)
            return;
        // FIXME forse dovrei controllare scenarioNome
        console.log(scenario)
        if (scenarioNome.startsWith(scenario.nome))
            Api.scenari.showScenarioAfterToggling(scenario, activating,true);
    }

    static sendWebScenarioAttivoAutomatico = scenarioNome => {
        Api.scenari.showScenarioAfterToggling(scenarioNome, true,true);
    }

    static overlayScenariAntifurto = stato => {
        Api.scenari.correctlySetAntifurto(stato,false,true);
    }
    // /api/scenari/registra/
    static recordingRecordScenario = null;
    static learnINBtnRecordScenario = null;
    /**
     * starts/stop the recording of a scenario
     * @param {Boolean} recording true to start the recording, false to stop it
     * @returns {*} null if the recording was started or stopped
     */
    static recordScenario = (recording, learnINBtn) => {
        if (recording == null)
            throw new Error("recording must be defined");
        if (learnINBtn == null)
            throw new Error("learnINBtn must be defined");
        Api.recordingRecordScenario = recording;//true se voglio registrare
        Api.learnINBtnRecordScenario = learnINBtn;
        console.log(learnINBtn)

        const INBtn = learnINBtn.nome;
        const topic = `from/${mqtt_tree}gpio/${INBtn.toUpperCase()}`;
        const message = `{"event":1}`;
        sendMessage(topic, message);
    }
    static retriesRecordScenario = 0;
    static sendWebScenarioAutoma = stato => {
        if(isNaN(stato))
            return;
        const recording = Api.recordingRecordScenario;
        // const statoAspettato = recording ? 1 : 0;
        if (recording != null && stato !== recording && Api.retriesRecordScenario < 3) {
            Api.retriesRecordScenario++;

            const INBtn = Api.learnINBtnRecordScenario.nome;
            const topic = `from/${mqtt_tree}gpio/${INBtn.toUpperCase()}`;
            const message = `{"event":1}`;
            sendMessage(topic, message);
            return;
        }
        Api.recordingRecordScenario = null;
        Api.learnINBtnRecordScenario = null;
        Api.retriesRecordScenario = 0;
        Api.scenari.apiCallbackRecordScenario(recording);
    }
    // /api/scenari/salva/
    /**
     * tells the server to save the currently recorded scenario
     * @returns {*} the scenario if it was saved
     */
    static saveScenario = nome => {
        if (nome == null)
            throw new Error("nome must be defined");

        const topic = `to/${mqtt_tree}scenari/salva`;
        const message = JSON.stringify({nome});
        sendMessage(topic, message);
    };
    static sendWebSavedScenario = scenario => {
        Api.scenari.saveScenarioCallback(scenario);
    }
    // /api/scenari/conf
    static sendScenariConfiguration = conf => {
        const topic = `conf/${mqtt_tree}scenari`;
        const message = JSON.stringify(conf);
        sendMessage(topic, message);
    }
    static sendWebConfigurationScenari = conf => {
        Api.scenari.updateConfiguration(conf);
    }
    // /api/antifurto/
    /**
     * gets everything about the antifurto (stato, allarme, attenzione (valore progress bar), soglia, sensori)
     * @returns {*} the antifurto's values
     */
    static getAntifurto = () => {
        const topic = `rpc/${mqtt_tree}antifurto`;
        const message = `{"request":"description"}`;
        sendMessage(topic, message);
    };
    static sendWebGetAntifurto = antifurto => {
        Api.antifurto.mostraAntifurtoServer(antifurto);
    }
    // /api/antifurto/stato/
    static payloadSetAntifurtoStatus = null;
    /**
     * sets the status of the antifurto
     * @param {Boolean} payload {previousStatus, fromScenari, fromServer}
     * @returns {*} null if the status was changed
     */
    static setAntifurtoStatus = payload => {
        if (payload == null)
            throw new Error("payload must be defined");
        // Api.newStatusSetAntifurtoStatus = newStatus;
        // Api.antifurtoINbtnSetAntifurtoStatus = antifurtoINbtn;
        Api.payloadSetAntifurtoStatus = payload;
        const INBtn = Api.antifurto?.attivaAntifurtoINbtn != null ? Api.antifurto.attivaAntifurtoINbtn.nome : Api.scenari?.antifurtoINbtn.nome;

        const topic = `from/${mqtt_tree}gpio/${INBtn.toUpperCase()}`;
        const message = `{"event":1}`;
        sendMessage(topic, message);
    };
    static retriesSetAntifurtoStatus = 0;
    static sendWebSetAntifurtoStatus = stato => {
        if(stato === NaN)
            return;
        const newStatus = Api.antifurto?.status != null ? !Api.antifurto.status : !Api.scenari?.antifurtoStatus;
        const expectedStatus = newStatus ? 2 : 0;

        const nomeIN = Api.antifurto?.attivaAntifurtoINbtn != null ? Api.antifurto.attivaAntifurtoINbtn.nome : Api.scenari?.antifurtoINbtn.nome;
        if (stato !== expectedStatus && Api.retriesSetAntifurtoStatus < 3) {
            Api.retriesSetAntifurtoStatus++;

            const topic = `from/${mqtt_tree}gpio/${nomeIN.toUpperCase()}`;
            const message = `{"event":1}`;
            sendMessage(topic, message);
            return;
        }
        const payload = Api.payloadSetAntifurtoStatus;
        // Api.newStatusSetAntifurtoStatus = null;
        // Api.antifurtoINbtnSetAntifurtoStatus = null;
        Api.payloadSetAntifurtoStatus = null;
        Api.retriesSetAntifurtoStatus = 0;
        try{
            Api.antifurto?.showChangeAntifurtoStatus(payload?.previousStatus ? payload?.previousStatus : Api.antifurto?.status, newStatus, payload?.fromScenari ? payload?.fromScenari : false, payload?.fromServer ? payload?.fromServer : true);
        } catch (error) {
            if(error.message !== "Antifurto.showChangeAntifurtoStatus is not a function")
                throw error;
            else Api.scenari?.showChangeAntifurtoStatus(newStatus, payload?.fromServer ? payload?.fromServer : true);
        }
    }

    static overlayAntifurtoAntifurto = stato => {
        Api.antifurto.activate(stato,false,true);
    }
    // /api/antifurto/allarme/
    static fixedCallbackStatoAllarme = null;
    /**
     * sets the status of the antifurto's alarm
     * @param {Boolean} allarme the new status of the antifurto's alarm
     * @returns {*} null if the status was changed
     */
    static setAntifurtoAllarme = allarme => {
        // const booleanAlarm = allarme == 'true';
        if (allarme == null || Api.fixedCallbackStatoAllarme == null)
            return;
        Api.fixedCallbackStatoAllarme(allarme);
    };
    // /api/antifurto/attenzione/
    static fixedCallbackValoreAttenzione = null;
    /**
     * sets the status for the progress bar of the antifurto's alarm
     * @param {Number} attenzione the new value of the antifurto's attention
     * @returns {*} null if the value was changed
     */
    static setAntifurtoAttenzione = attenzione => {
        if (attenzione == null || isNaN(parseInt(attenzione)) || Api.fixedCallbackValoreAttenzione == null)
            return;
        const val = parseInt(attenzione);
        Api.fixedCallbackValoreAttenzione(val);
    };
    // /api/antifurto/soglia/
    static sogliaSetAntifurtoSoglia = null;
    static fromServerSetAntifurtoSoglia = null;
    static callbackSetAntifurtoSoglia = null;
    /**
     * sets the value of the antifurto's threshold
     * @param {Number} soglia the user's value of the antifurto's threshold
     * @returns {*} null if the value was changed
     */
    static setAntifurtoSoglia = (soglia, fromServer) => {
        if (soglia == null || isNaN(parseInt(soglia)))
            throw new Error("soglia must be a set integer");
        const val = parseInt(soglia);
        if (val < 0 || val > 100)
            throw new Error("soglia must be between 0 and 100");
        if (fromServer == null)
            throw new Error("fromServer must be defined");
        Api.sogliaSetAntifurtoSoglia = val;
        Api.fromServerSetAntifurtoSoglia = fromServer;

        const topic = `conf/${mqtt_tree}antifurto/soglia`;
        const message = `{"soglia":${val}}`;
        sendMessage(topic, message);
    };
    static sendWebSoglia = soglia => {
        const expectedSoglia = Api.sogliaSetAntifurtoSoglia;
        if (expectedSoglia !== soglia) {
            console.log('soglie differenti', { expectedSoglia, soglia });
        }
        const fromServer = Api.fromServerSetAntifurtoSoglia;
        Api.sogliaSetAntifurtoSoglia = null;
        Api.fromServerSetAntifurtoSoglia = null;
        Api.antifurto.showEditsSoglia(soglia, fromServer);
    }
    // /api/antifurto/conf
    static sendAntifurtoConfiguration = json => {
        const topic = `conf/${mqtt_tree}antifurto`;
        const message = JSON.stringify(json);
        sendMessage(topic, message);
    }
    static sendWebConfigurationAntifurto = conf => {
        Api.antifurto.updateConfiguration(conf);
    }

    static sendSensoreConfiguration = json => {
        const topic = `conf/${mqtt_tree}antifurto/sensore`;
        const message = JSON.stringify(json);
        sendMessage(topic, message);
    }
    static sendWebConfigurationSensore = conf => {
        Api.antifurto.updateSensoreConfiguration(conf);
    }



    static testSensore = sensore => {//usato da sensori.js
        const topic = `from/${mqtt_tree}gpio/${sensore.toUpperCase()}`;
        const message = `{"event":1}`;
        sendMessage(topic, message);
    }
}

export default Api;
