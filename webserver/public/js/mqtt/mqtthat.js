"use strict";

import Api from "./api.js";

// Create a client instance
const mqtt_domain = "gruppo2";
const mqtt_subdomain = "luci";
const mqtt_tree = mqtt_domain + "/" + mqtt_subdomain + "/";
export { mqtt_tree };
const d = new Date();
const clientid = "browser" + d.getTime();
const mqtthost = 'localhost';
// const mqtthost = 'smartcity-challenge.edu-al.unipmn.it';

const client = new Paho.MQTT.Client(mqtthost, 9001, clientid);
// const client = new Paho.MQTT.Client(mqtthost, 8883, clientid);

// set callback handlers
client.onConnectionLost = onConnectionLost;
client.onMessageArrived = onMessageArrived;

const connectOptions = {
    onSuccess: onConnect,
    userName: "gruppo2",
    password: "funziona",
    useSSL: true,
    // cleanSession: false,
    onFailure: onFailure,
};
// connect the client
client.connect(connectOptions);

let isConnected = false;

function sendMessage(topic, message, retryNum = 0) {
    if (isConnected) {
        const message_ = new Paho.MQTT.Message(message);
        message_.destinationName = topic;
        client.send(message_);
    } else if (retryNum < 3) {
        setTimeout(() => {
            sendMessage(topic, message, retryNum++);
        }, 1000);
    } else {
        console.log('impossibile inviare il messaggio')
        throw new Error('impossibile inviare il messaggio');
    }
}

export { sendMessage };

let hasDisconnected = false;

// called when the client connects
function onConnect() {
    // Once a connection has been made, make a subscription and send a message.
    // const subscriptionFrom = `from/${mqtt_tree}gpio`;
    // const subscriptionTo = `to/${mqtt_tree}webapp/#`;
    // client.subscribe(`from/${mqtt_tree}gpio/#`);
    client.subscribe(`from/${mqtt_tree}luci/#`);
    client.subscribe(`from/${mqtt_tree}scenari/#`);
    client.subscribe(`from/${mqtt_tree}antifurto/#`);
    // client.subscribe(subscriptionTo);
    isConnected = true;
    // tell every module that we're connected
    Api.isConnected = true;
    if(hasDisconnected){
        Api.onConnect();
        hasDisconnected = false;
    }
}

let dateConLost = null;
const maxTime = 1200000;
let retry = true;
function onFailure(message) {
    console.log("onFailure", message);
    dateConLost == null ? dateConLost = new Date() : null;
    let time = Math.abs(new Date().getTime() - dateConLost.getTime());
    if (retry && (time < maxTime)) {
        isConnected = false;
        Api.isConnected = false;
        hasDisconnected = true;
        client.connect(connectOptions);
    }
    if (time >= maxTime) {
        retry = false;
        console.log('Tentativo di riconnessione fallito');
    }
}

// called when the client loses its connection
function onConnectionLost(responseObject) {
    console.log("onConnectionLost:", responseObject);
    isConnected = false;
    Api.isConnected = false;
    hasDisconnected = true;
    setTimeout(() => {
        client.connect(connectOptions);
    }, 1000);
}
// called when a message arrives
function onMessageArrived(message) {
    const topic_name = message.topic.split("/");
    if (topic_name.length < 4) return;
    const from = topic_name.pop().toLowerCase();//get the last element
    const name_content = message.payloadString.split(":");
    const rawValue = cleanItem(name_content[1]);
    const valueInt = parseInt(rawValue);
    const messageJSON = JSON.parse(message.payloadString);
    if (message.topic.startsWith(`from/${mqtt_tree}luci`)) {
        if (from === 'luci')
            Api.sendDataLuciWeb(messageJSON);
        else if (from === 'new')
            Api.sendWebNewLuci(messageJSON);
        else if (from === 'sensore')
            Api.sendWebNewSensore(messageJSON);
        else if (from === 'luce')
            Api.sendLuciStatoWeb(messageJSON);
        return;
    }
    if (message.topic.startsWith(`from/${mqtt_tree}scenari`)) {
        if (from === 'scenari') {
            if (messageJSON.status != null) {//escludo le luci
                console.log('ricevuto roba inutile', messageJSON)
                return;
            }
            if (messageJSON.learnINbtn != null) {
                Api.sendWebConfigurationScenari(messageJSON);
                return;
            }
            //else it's RPC
            Api.sendScenariWeb(messageJSON);
        }
        else if (from === 'attiva') {
            console.log('cambiostatoscenario', rawValue)
            Api.sendWebNewScenarioStatus(rawValue);
        }
        else if (from === 'learn')
            Api.sendWebScenarioAutoma(rawValue == 'true');
        else if (from === 'antifurto')
            Api.overlayScenariAntifurto(rawValue == 'true');
        else if (from === 'salva')
            Api.sendWebSavedScenario(messageJSON);
        else if (from === 'attivazioneautomatica') {
            console.log('leggiattivazioneautomaticascenari', rawValue)
            Api.sendWebScenarioAttivoAutomatico(rawValue);
        }
        return;
    }
    if (message.topic.startsWith(`from/${mqtt_tree}antifurto`)) {
        if (from === 'antifurto' && topic_name[topic_name.length - 1] === 'antifurto')
            // qui dopo aver dato set stato (scenari -> cmd -> event -> ... -> qui)
            Api.overlayAntifurtoAntifurto(rawValue == 'true');
        else if (from === 'antifurto') {
            // dopo rpc per descrizione
            if (messageJSON.antifurto != null)
                Api.sendWebGetAntifurto(messageJSON);
            else //it's after the conf
                Api.sendWebConfigurationAntifurto(messageJSON);
        } else if (from === 'valore')
            Api.setAntifurtoAttenzione(valueInt);
        else if (from === 'allarme')
            Api.setAntifurtoAllarme(rawValue == 'true');
        else if (from === 'soglia')
            Api.sendWebSoglia(valueInt);
        else if (from === 'sensore')
            Api.sendWebConfigurationSensore(messageJSON);
        return;
    }
    console.log('impossibile', message.topic, message.payloadString);
}

// funzioni del prof
function cleanItem(item) {
    let citem = "";
    for (let i = 0; i < item.length; i++)
        if (item[i] != '"' && item[i] != '}' && item[i] != '{' && item[i] != '\n')
            citem += item[i];
    return citem;
}