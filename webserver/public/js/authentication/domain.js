'use strict';

class Domain {
    constructor(nome, stato, admin) {
        this.nome = nome; // nome del dominio
        this.stato = stato; // on/off
        this.admin = admin; // true se l'utente attualmente loggato e' amministratore di questo dominio, false altrimenti
    }

    /**
     * Costruisce un oggetto Domain partendo dall'oggetto passato in input. 
     * @param {*} json oggetto dal quale si vuole creare un oggetto Domain.
     * @returns l'oggetto Domain creato.
     */
     static from(json) {  
        const p = Object.assign(new Domain(), json); 
        return p;
    }

}

export default Domain;