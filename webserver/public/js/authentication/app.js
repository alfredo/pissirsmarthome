'use strict';
import {createRowDomain} from '../templates/domains-template.js';
import { logoutKeycloak } from './script.js';
import {createNewUser, createNewService, createControlServices} from '../templates/create-new-domain-template.js';
import RequestToDomain from './requests-to-domain.js';

class App {
    constructor(myDomains) {
        // this.myDomains = myDomains;
        this.showAllDomains(myDomains);
        this.fillModal();
        this.performLogout();
    }

    showAllDomains(domainsToShow) {
        const addHere = document.getElementById('table-row-domains');
        for(const d of domainsToShow) {
            this.showSingleDomain(d,addHere);
        }
    }

    showSingleDomain(domainObject,addHere){
        const container = addHere == null ? document.getElementById('table-row-domains') : addHere;
        const tr = document.createElement('tr');
        tr.innerHTML = createRowDomain(domainObject);
        const plusRow = container.lastElementChild;
        container.insertBefore(tr, plusRow);
        if(domainObject.admin) {
            const toggle = tr.querySelector('.toggle-button');
            this.statoDomainToggle(domainObject, toggle);
            const deleteDomain = tr.querySelector('.fa-trash');
            deleteDomain.addEventListener('click', async () => {
                const resultDelete = await RequestToDomain.deleteDomain(domainObject);
                if(resultDelete) {
                    container.removeChild(tr);
                }
                else {
                    // throw new Error('Impossibile eliminare il dominio, provare piu\' tardi');
                    this.showAlert('Errore eliminazione', 'Impossibile eliminare il dominio, provare piu\' tardi', false);
                }
            });
        }
        const a = tr.querySelector('th > h4 > a');
        a.addEventListener('click', (event) => {
            event.preventDefault();
            sessionStorage.setItem('domain', domainObject.nome);
            window.location.href = a.href;
        });
    }


    statoDomainToggle(domain, toggle) {
        toggle.addEventListener('click', async (event) => {
            event.preventDefault();
            const stop = toggle.classList.contains('active');
            const linkDomain = toggle.parentElement.parentElement.previousElementSibling.querySelector('a');
            if(stop) {
                // toggle attiva. Se clicco richiedo che il dominio sia fermato
                const resultStop = await RequestToDomain.stopDomain(domain);
                if(resultStop) {
                    toggle.classList.remove('active');
                    linkDomain.setAttribute('href','#');
                }
                else {
                    // throw new Error('Impossibile fermare il dominio, provare piu\' tardi');
                    this.showAlert('Errore', 'Impossibile fermare il dominio, provare piu\' tardi', false);
                }
            }
            else {
                const resultStart = await RequestToDomain.startDomain(domain);
                if(resultStart) {
                    toggle.classList.add('active');
                    linkDomain.setAttribute('href','/secured/home/');

                }
                else {
                    // throw new Error('Impossibile far partire il dominio, provare piu\' tardi');
                    this.showAlert('Errore', 'Impossibile far partire il dominio, provare piu\' tardi', false);
                }
            }
        });
    }


    async fillModal() {
        const allServices = await RequestToDomain.getAllServices();
        const div = document.createElement('div');
        div.id = "checkbox-div";
        div.classList.add('pt-4');
        div.classList.add('pb-2');
        for(const s of allServices) {
            div.insertAdjacentHTML('beforeend', createNewService(s));
        }

        const modalBody = document.getElementById('modal-body-new-domain');
        modalBody.appendChild(div);
        div.insertAdjacentHTML('afterend', createControlServices());
        modalBody.insertAdjacentHTML('beforeend', createNewUser());

        let userNum = 1;// una riga e' gia' presente
        const addUserButton = document.getElementById('add-user-button');
        addUserButton.addEventListener('click', () => {
            modalBody.insertAdjacentHTML('beforeend', createNewUser(userNum));
            userNum++;
        });


        const form = document.getElementById('new-domain-form');
        const cancelForm = document.getElementById('cancel-button');
        form.addEventListener('submit', async (event) => {
            event.preventDefault();

            let errors = 0;
            const domainNameInput = document.getElementById('nuovoDominio');
            const domainName = domainNameInput.value.trim();
            if(domainName === ''){
                errors++;
                domainNameInput.classList.add('is-invalid');
                domainNameInput.classList.remove('is-valid');
                document.getElementById('valid-domainName').innerHTML = '';
                document.getElementById('invalid-domainName').innerHTML = 'Inserire un nome';
            }
            else {
                domainNameInput.classList.add('is-valid');
                domainNameInput.classList.remove('is-invalid');
                document.getElementById('valid-domainName').innerHTML = 'Ok';
                document.getElementById('invalid-domainName').innerHTML = '';
            }

            const topicsArr = [ {role:"A", topic: `+/${domainName}/#`} ];
            const checkboxarr = document.querySelectorAll("input.form-check-input");
            const checks = [];  // array contentente i nomi dei servizi che l'utente vuole nel dominio che sta creando
            for(const c of checkboxarr){
                if(c.checked) {
                    const el = (c.value == 'bbgpio' ? 'gpio' : (c.value == 'AinDout_Arduino1wifi' ? 'arduino' : c.value));
                    checks.push(el);
                    // change bbgpio to gpio to enable the correct topics
                    topicsArr.push({role:"U", topic: `to/${domainName}/luci/${el}/#`});
                    topicsArr.push({role:"U", topic: `from/${domainName}/luci/${el}/#`});
                    topicsArr.push({role:"U", topic: `rpc/${domainName}/luci/${el}/#`});
                }
            }

            const checkboxDiv = document.getElementById('checkbox-div');

            if(checks.length === 0){
                errors++;
                checkboxDiv.classList.add('is-invalid');
                checkboxDiv.classList.remove('is-valid');
                document.getElementById('valid-services').innerHTML = '';
                document.getElementById('invalid-services').innerHTML = 'Selezionare almeno un servizio';
            }
            else if(checks.includes('scenari') && !checks.includes('luci')) {
                // scenari deve avere anche luci per funzionare
                errors++;
                checkboxDiv.classList.add('is-invalid');
                checkboxDiv.classList.remove('is-valid');
                document.getElementById('valid-services').innerHTML = '';
                document.getElementById('invalid-services').innerHTML = 'Non e\' possibile avere il microservizio degli scenari senza il microservizio delle luci';
            }
            else if ((checks.includes('luci') || checks.includes('scenari') || checks.includes('antifurto')) && !checks.includes('gpio')) {
                // i nostri microservizi necessitano della bbgpio
                errors++;
                checkboxDiv.classList.add('is-invalid');
                checkboxDiv.classList.remove('is-valid');
                document.getElementById('valid-services').innerHTML = '';
                document.getElementById('invalid-services').innerHTML = 'Non e\' possibile avere il microservizio delle luci, degli scenari o dell\'antifurto senza il  bbgpio';
            }
            else if (checks.includes('luci') && !checks.includes('arduino')) {
                // le luci necessitano dell'arduino
                errors++;
                checkboxDiv.classList.add('is-invalid');
                checkboxDiv.classList.remove('is-valid');
                document.getElementById('valid-services').innerHTML = '';
                document.getElementById('invalid-services').innerHTML = 'Non e\' possibile avere il microservizio delle luci senza l\'arduino';
            }
            else {
                // tutto ok
                checkboxDiv.classList.add('is-valid');
                checkboxDiv.classList.remove('is-invalid');
                document.getElementById('valid-services').innerHTML = 'Ok';
                document.getElementById('invalid-services').innerHTML = '';
            }


            let i=0;
            const utenti = [];
            while(i<userNum){
                const nomeInput = document.getElementById(`nuovoUtente${i}`);
                const nome = nomeInput.value;

                if(nome==='') {
                    errors++;
                    nomeInput.classList.add('is-invalid');
                    nomeInput.classList.remove('is-valid');
                    document.getElementById(`valid-userName${i}`).innerHTML = '';
                    document.getElementById(`invalid-userName${i}`).innerHTML = 'Inserire il nome dell\'utente';
                }
                else {
                    nomeInput.classList.add('is-valid');
                    nomeInput.classList.remove('is-invalid');
                    document.getElementById(`valid-userName${i}`).innerHTML = 'Ok';
                    document.getElementById(`invalid-userName${i}`).innerHTML = '';
                }

                const passwordInput = document.getElementById(`passwordUtente${i}`);
                const password = passwordInput.value;
                if(password==='') {
                    errors++;
                    passwordInput.classList.add('is-invalid');
                    passwordInput.classList.remove('is-valid');
                    document.getElementById(`valid-userPassword${i}`).innerHTML = '';
                    document.getElementById(`invalid-userPassword${i}`).innerHTML = 'Inserire la password per l\'utente';
                }
                else {
                    passwordInput.classList.add('is-valid');
                    passwordInput.classList.remove('is-invalid');
                    document.getElementById(`valid-userPassword${i}`).innerHTML = 'Ok';
                    document.getElementById(`invalid-userPassword${i}`).innerHTML = '';
                }

                const ruoloInput = document.getElementById(`ruoloUtente${i}`);
                const ruolo = ruoloInput.value;
                if(ruolo === 'unselected') {
                    errors++;
                    ruoloInput.classList.add('is-invalid');
                    ruoloInput.classList.remove('is-valid');
                    document.getElementById(`valid-userRole${i}`).innerHTML = '';
                    document.getElementById(`invalid-userRole${i}`).innerHTML = 'Specificare il ruolo dell\'utente';
                }
                else {
                    ruoloInput.classList.add('is-valid');
                    ruoloInput.classList.remove('is-invalid');
                    document.getElementById(`valid-userRole${i}`).innerHTML = 'Ok';
                    document.getElementById(`invalid-userRole${i}`).innerHTML = '';
                }

                utenti.push({
                    user: nome,
                    role: ruolo,
                    passwd: password
                });
                i++;
            }

            if(errors > 0) {
                return;
            }
            const json = {
                domain: domainName,
                services: checks,
                users: utenti,
                topics: topicsArr
            };
            cancelForm.click();

            const resultInstall = await RequestToDomain.createNewDomain(json);

            if(resultInstall) {
                const domainCreated = {
                    nome: domainName,
                    stato: 0,
                    admin: true
                };
                this.showSingleDomain(domainCreated);
            }
            else {
                // throw new Error('Impossibile creare il dominio, provare piu\' tardi');
                this.showAlert('Dominio esistente','Impossibile creare il dominio, provare con un altro nome', false);
            }

        });


        cancelForm.addEventListener('click', () => {
            form.reset();
            document.getElementById('nuovoDominio').classList.remove('is-valid');
            document.getElementById('nuovoDominio').classList.remove('is-invalid');
            document.getElementById('valid-domainName').innerHTML = '';
            document.getElementById('invalid-domainName').innerHTML = '';

            document.getElementById('valid-services').innerHTML = '';
            document.getElementById('invalid-services').innerHTML = '';

            // serve userNum -1 perche' subito dopo aver creato la riga, userNum viene incrementato
            // si va fino a 0 perche' la prima riga viene mantenuta
            for(let i=(userNum-1); i>0; i--){
                const divRow = document.getElementById(`utente${i}`);
                const parent = divRow.parentNode;
                parent.removeChild(divRow);
            }
            userNum = 1;
            const userRow = document.getElementById(`utente${userNum-1}`);
            const inputs = userRow.querySelectorAll('input');
            for(const input of inputs){
                input.classList.remove('is-valid');
                input.classList.remove('is-invalid');
            }
            const feedback = [...userRow.querySelectorAll('div > div.valid-feedback')];
            feedback.push(...userRow.querySelectorAll('div > div.invalid-feedback'));
            for(const f of feedback){
                f.innerHTML = '';
            }
            const select = userRow.querySelector('select');
            select.classList.remove('is-valid');
            select.classList.remove('is-invalid');
        });
    }



    performLogout() {
        const buttonLogout = document.getElementById('button-logout');
        buttonLogout.addEventListener('click', async (event) => {
            event.preventDefault();
            logoutKeycloak();
        });
    }


    showAlert(head, body, green) {
        const alert = document.getElementById('alert-message');
        if(alert == null)
            throw new Error("Alert box non trovata. Errore: " + body);
        green ? alert.classList.add('alert-success') : alert.classList.add('alert-danger');
        alert.innerHTML = `
        <div class="d-flex justify-content-around container-fluid">
            ${green ? '<i class="fa-solid fa-check-circle fa-4x" aria-hidden="true"></i>' : '<i class="fa-solid fa-exclamation-circle fa-4x aria-hidden="true""></i>'}
            <div>
                <h4 class="alert-heading">${head}</h4>
                <p>${body}</p>
            </div>
        </div>`;//<i class="fa-solid fa-triangle-exclamation fa-fade"></i>
        alert.classList.remove('invisible');
        // dismiss the alert after 3 seconds
        setTimeout(() => {
            alert.classList.add('invisible');
            alert.classList.remove('alert-success');
            alert.classList.remove('alert-danger');
            alert.innerHTML = "";
        }, 3000);
    };

}

export default App;