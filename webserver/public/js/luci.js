"use strict";
import { luciToggleListener } from "./toggles.js";
import { makeElementBounce, showAlert } from "./alerts.js";
import Api from "./mqtt/api.js";
import Sensori from "./sensori.js";

const luceTemplate = {
    luogo: "",
    stato: false,
    id: "",
    "sensore-in": "",
    "luce-out": "",
};

class Luci {
    /**
     * the div where everything related to the Luci is stored
     */
    static luciContainer = document.getElementById('luciContainer');
    /**
     * the array where all of the user's luci are stored
     */
    static luciBucket = [];
    /**
     * describes how many lights have already been added
     */
    static index = 0;

    static sensoreMovimento = null;

    constructor(servicesArray) {
        Api.fixedCallbackLuciStato = Luci.switchLuceFromMqttEvent;
        Luci.init(servicesArray);
        Luci.modalListener();
    }

    static antifurtoClass;
    static scenariClass;
    static async init(servicesArray) {
        //import other modules
        if(servicesArray[0]){
            const antifurto = await import('./antifurto.js');
            this.antifurtoClass = antifurto.default;
        }
        if(servicesArray[1]){
            const scenari = await import('./scenari.js');
            this.scenariClass = scenari.default;
        }

        const luciBtn = document.getElementById('mainButtonContainer').children[0];
        luciBtn.addEventListener('click', () => {
            try{
                Luci.scenariClass?.toggleContainer(false);
            } catch (error) {
                if(error.message !== "Scenari.toggleContainer is not a function" && error.message !== "Cannot read properties of null (reading 'classList')")
                    throw error;
            }
            try{
                Luci.antifurtoClass?.toggleContainer(false);
            } catch (error) {
                if(error.message !== "Antifurto.toggleContainer is not a function" && error.message !== "Cannot read properties of null (reading 'classList')")
                    throw error;
            }
            Luci.toggleContainer(true);
        }, false);

        // listen for the closing modal
        const cancelBtn = document.querySelector('#luci-modal .btn.btn-secondary');
        cancelBtn.addEventListener('click', () => {
            // delay 0.5s to allow the modal to close (otherwise it's not pretty)
            setTimeout(() => {
                Luci.selectReset();
            }, 500);
        }, false);

        Luci.fillTable();
    }
    /**
     * shows and listens for the modal that allows the user to add a new light
     */
    static modalListener() {

        const selectStanza = document.getElementById('luogoLuce');
        const altroInput = document.getElementById('nuovaStanzaDIV');
        const altroClassList = altroInput.classList;
        const form = document.getElementById('search-form');
        const inputField = altroInput.querySelector('input');

        // show or hide the text input for the new room
        selectStanza.addEventListener('change', event => {
            event.preventDefault();
            const selected = event.target.value;

            if (selected === "altro") {
                altroClassList.remove('invisible');
                // set altroInput to required
                inputField.setAttribute('required', 'required');
            } else {
                altroClassList.add('invisible');
                // remove required from altroInput
                inputField.removeAttribute('required');
            }
        }, false);

        form.addEventListener('submit', event => {
            event.preventDefault();

            const selectedStanza = selectStanza.value;
            const selectIn = document.getElementById('scelta-porta-in');
            const selectedIn = selectIn.value;
            const selectOut = document.getElementById('scelta-porta-out');
            const selectedOut = selectOut.value;
            if (selectedStanza === "unselected" || selectedIn === "unselected" || selectedOut === "unselected") {
                alert("Selezionare i valori validi dai menu a tendina.");
                return;
            }

            let stanza = selectStanza.options[selectStanza.selectedIndex].text.trim(); // get selected option text
            if (selectedStanza === "altro") {
                const nuovaStanza = altroInput.querySelector('#nuovaStanza').value.trim();
                if (nuovaStanza == null || nuovaStanza == "" || nuovaStanza.toLowerCase() === "altro") {
                    alert("Inserire un nome valido per la stanza.");
                    return;
                }
                for (const stanza of Luci.luciBucket)
                    if (stanza.luogo === nuovaStanza) {
                        alert("Stanza già esistente");
                        return;
                    }
                stanza = nuovaStanza;
                // reset the select
                Luci.selectReset();
            }
            stanza = stanza[0].toUpperCase() + stanza.substring(1, stanza.length);
            // add stanza to table
            const luceJSON = Luci.createLight(stanza, false, selectedIn, selectedOut);
            Luci.sendLightToServer(luceJSON);

            // close modal
            const closeBtn = document.querySelector('#luci-modal .btn.btn-secondary');
            closeBtn.click();
        }, false);
    }
    /**
     * sends the light to the server and adds it to the table if the server responds with success
     * @param {luceTemplate} luce the light to be added to the table and the server
     */
    static sendLightToServer(luce) {
        // server send luce
        try {
            Api.makeNewLuci(luce);
        } catch (error) {
            if (error.error)
                showAlert("Errore di comunicazione.", error.error, false);
        }
    }

    static showCreatedLight(luce) {
        Luci.showNewLight(luce);
        showAlert(`${luce.luogo} creato`, "La nuova luce e' stata creata, usala subito!", true);
    }
    /**
     * creates a new light and adds it to the bucket
     * @param {String} luogo the name of the room
     * @param {Boolean} stato the status of the light
     * @returns {luceTemplate} the new light
     */
    static createLight(luogo, stato, valIn, valOut) {
        if(luogo == null)
            throw new Error("luogo non impostato");
        if (valIn == null || valOut == null || valIn.trim() == "" || valOut.trim() == "")
            throw new Error("i sensori in/out devono essere impostati");
        const luce = Object.assign({}, luceTemplate);
        luce.luogo = luogo[0].toUpperCase() + luogo.substring(1, luogo.length).toLowerCase();
        luce.stato = stato != null ? stato : false;
        luce.id = `luce-${Luci.index}`;
        luce["sensore-in"] = valIn.trim();
        luce["luce-out"] = valOut.trim();
        Luci.luciBucket.push(luce);
        Luci.index++;
        return luce;
    }
    /**
     * shows a new light in the table after creating it
     * @param {luceTemplate} luceJSON the light to show
     */
    static showNewLight(luceJSON) {
        const selectIn = document.getElementById('scelta-porta-in');
        const sensoreIn = selectIn.value !== "unselected" ? selectIn.value : luceJSON["sensore-in"];
        const selectOut = document.getElementById('scelta-porta-out');
        const sensoreOut = selectOut.value !== "unselected" ? selectOut.value : luceJSON["luce-out"];
        let sensoreInJSON = null;
        let sensoreOutJSON = null;
        try {
            if (selectIn.children.length > 1){
                sensoreInJSON = Sensori.claimSensore(sensoreIn);
                // if(sensoreInJSON != null)
                //     Luci.removeSensoreOption(selectIn, sensoreInJSON);
            }
            if (sensoreInJSON != null && selectOut.children.length > 1){
                sensoreOutJSON = Sensori.claimSensore(sensoreOut);
                // if(sensoreOutJSON != null)
                //     Luci.removeSensoreOption(selectOut, sensoreOutJSON);
            }
            if(sensoreInJSON == null || sensoreOutJSON == null)
                return;
        } catch (error) {
            if (error.message === "Errore impossibile")
                showAlert("Errore", "Impossibile rimuovere il sensore dai menu a tendina.", false);
            else showAlert("Errore", error.message, false);
            // se ho un errore, reimposto tutto
            //Luci.resetBothSelectForSensors();
            Sensori.liberaSensore(sensoreInJSON);
            Sensori.liberaSensore(sensoreOutJSON);
            return;
        }
        let utilizzo = '';
        if(sensoreInJSON?.utilizzo == null)
            utilizzo += sensoreInJSON.utilizzo = `Interruttore [${sensoreInJSON.nome}]`;
        utilizzo = utilizzo !== '' ? utilizzo + '<br />' : utilizzo;
        if(sensoreOutJSON?.utilizzo == null)
            utilizzo += sensoreOutJSON.utilizzo = `Luce [${sensoreOutJSON.nome}]`;
        utilizzo += `<br />per ${luceJSON.luogo}`;
        const luce = luceJSON.id == null ? (  Luci.createLight(luceJSON.luogo, luceJSON.stato, luceJSON["sensore-in"], luceJSON["luce-out"])  ) : luceJSON;
        const table = document.getElementById('table-row-luci');
        const row = document.createElement('tr');

        row.innerHTML = `
        <th scope="row" id="${luce.id}">${luce.luogo}</th>
        <td>
            <div class="switch-container no-box-sizing">
                <div class="toggle-button no-box-sizing ${luce.stato ? 'active' : ''}" data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-html="true" title="${utilizzo}" data-sensore-in="${JSON.stringify(sensoreInJSON)}" data-sensore-out="${JSON.stringify(sensoreOutJSON)}">
                    <div class="inner-circle no-box-sizing" />
                </div>
            </div>
        </td>`;
        const lastRow = table.lastElementChild;
        table.insertBefore(row, lastRow);
        Sensori.initializaTooltips();
        // add listener for switch
        const toggle = row.querySelector('.toggle-button');
        luciToggleListener(luce,toggle);
        // remove options from the select of the place
        const select = document.getElementById('luogoLuce');
        const options = select.options;
        const luogoLowercase = luce.luogo.toLowerCase();
        for (const option of options) {
            if (option.text.toLowerCase() === luogoLowercase) {
                select.removeChild(option);
                break;
            }
        }
    }
    /**
     * rimuove il sensore dalle opzioni per l'utente (gia' fatto dalla classe Sensori)
     * @param {*} selectNode
     * @param {*} sensore
     * @returns
     */
     /*static removeSensoreOption(selectNode,sensore){
        if(sensore == null){
            throw new Error("Nome sensore non impostato");
        }
        const options = selectNode.querySelectorAll("option");
        let removed = false;
        for(const option of options){
            if(option.value === sensore.nome){
                selectNode.removeChild(option);
                removed = true;
                break;
            }
        }
        if(!removed){
            // Sensori.liberaSensore(nome);
            console.log('sensore non rimosso')
            throw new Error("Errore impossibile");
        }
    }*/

    static switchLuceFromMqttEvent(mqttObject){
        // mqttObject == {output:outVal,stato};
        // from the OUT and the sensors active, find the light and change the status
        mqttObject.output = mqttObject.output.toUpperCase();
        const luce = Luci.luciBucket.find(luce => luce["luce-out"].toUpperCase() === mqttObject.output);
        if(luce == null){
            console.log("luce non trovata");
            return;
        }
        luce.stato = mqttObject.stato;
        const toggle = document.getElementById(luce.id).nextElementSibling.querySelector('.toggle-button');
        luce.stato ? toggle.classList.add('active') : toggle.classList.remove('active');
    }
    /**
     * resets the select menu used to add a new light
     */
    static selectReset() {
        const select = document.getElementById('luogoLuce');
        const altroInput = document.getElementById('nuovaStanzaDIV');
        const inputField = altroInput.querySelector('input');

        select.selectedIndex = 0;
        altroInput.classList.add('invisible');
        inputField.removeAttribute('required');
        inputField.value = "";
    }
    /**
     * fills the table with the user's luci from the server
     */
    static fillTable() {
        // server get all lights
        try {
            if(!Api.isConnected){
                setTimeout(() => Luci.fillTable(), 2500);
                return;
            }
	        Sensori.showAvailableOptions();
            Api.getAllLuci();
        } catch (error) {
            if (error.error)
                showAlert("Errore di comunicazione", error.error, false);
            else if (error.message)
                showAlert("Errore", error.message, false);
        }
    }

    static luciFromMqtt(datiVari){
        if(Luci.sensoreMovimento != null)
            return;
        /*
        {
        "lamp0":
            {
            "nome": "Garage",
            "input": "IN6",
            "output": "OUT4",
            "stato":false
            },
        "lamp1":
            {
            "nome": "Garage",
            "input": "IN6",
            "output": "OUT4",
            "stato":false
            },
        "sensM":"IN7"
        }
        */

        let index = 0;
        for(const luce in datiVari){
            if(luce === "sensM"){
                const sensore = Sensori.claimSensore(datiVari["sensM"]);
                sensore.utilizzo = `Sensore di presenza per le luci [${sensore.nome}]`;
                Luci.sensoreMovimento = sensore;
                continue;
            }
            // qui mi occupo delle luci
            const luceObj = datiVari[`lamp${index}`];
            // const luceInput = Sensori.claimSensore(luceObj.input);
            // const luceOutput = Sensori.claimSensore(luceObj.output);
            // questi 2 sono gia' fatti da showNewLight
            const luceCreated = Luci.createLight(luceObj.nome, luceObj.stato, luceObj.input, luceObj.output);
            Luci.showNewLight(luceCreated);
            index++;
        }
    }

    static freeAll(){
        Sensori.liberaSensore(Luci.sensoreMovimento);
        Luci.sensoreMovimento = null;
        // for each light in luciBucket, free the sensors
        for(const luce of Luci.luciBucket){
            Sensori.liberaSensore(luce["sensore-in"]);
            Sensori.liberaSensore(luce["luce-out"]);
        }
        Luci.luciBucket = [];
        // delete all the rows in the UI
        const tableBody = document.getElementById('table-row-luci');
        // delete all rows except the last one
        while(tableBody.childElementCount > 1){
            tableBody.removeChild(tableBody.firstElementChild);
        }
        Luci.index = 0;
    }

    static updateStatusAfterMqtt(luci){
        for (const luce of luci) {
            const table = document.getElementById('table-row-luci');
            const luceJSON = Luci.luciBucket.find(old_luce => old_luce.out === luce.out);
            const th = table.querySelector(`#${luceJSON.id}`);
            const td = th.nextElementSibling;
            if(luce.stato !== luceJSON.stato){
                // click the toggle
                const toggle = td.querySelector('.toggle-button');
                toggle.click();
            }
        }
    }
    /**
     * toggle the visibility of the luci container
     * @param {Boolean} visible
     */
    static toggleContainer(visible) {
        if (visible) {
            Luci.luciContainer.classList.remove('invisible');
            if (Luci.luciBucket.length === 0) {
                // make the + button bounce
                const plusBtn = Luci.luciContainer.querySelector('.fa-circle-plus');
                makeElementBounce(plusBtn);
            }
        } else {
            Luci.luciContainer.classList.add('invisible');
        }
    }

    /*static showCreatedSensore(sensore) {//TODO used by Api
        if(Luci.sensoreMovimento != null)
            Sensori.liberaSensore(Luci.sensoreMovimento.nome);
        const claimedSensore = Sensori.claimSensore(sensore.sensM);
        claimedSensore.utilizzo = `Sensore di presenza per le luci [${claimedSensore.nome}]`;
        Sensori.showSensore(claimedSensore);
        Luci.sensoreMovimento = claimedSensore;
    }*/
}

export default Luci;
