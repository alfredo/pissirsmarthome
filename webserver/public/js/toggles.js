"use strict";
// import Scenari from './scenari.js';
import { showAlert, makeElementBounce } from './alerts.js';
import Api from './mqtt/api.js';

/**
 * edits the modal used for activating/deactivating a scenario when the user clicks on a toggle
 * @param {HTMLElement} toggle the toggle to move
 * @param {string} scenarioID the id of the scenario which is being toggled
 */
function scenariToggleListener(toggle, scenarioID) {
    const registerBtn = document.getElementById('scenari-registra');
    toggle.addEventListener('click', async event => {
        event.preventDefault();
        if(registerBtn.innerText !== "Registra"){//sto registrando
            showAlert("Attenzione", "Stai registrando uno scenario, termina la registrazione per attivare lo scenario.", false);
            makeElementBounce(registerBtn);
            return;
        }
        // select the option from the dropdown
        const scenari = await import('./scenari.js');
        scenari.default.addNameHere.value = scenarioID;
        // change text of the modal based on toggle state
        const modalBody = document.getElementById('attiva-scenari-modal').querySelector('.modal-body');
        const children = modalBody.children;
        //p, fieldset, p.more-margin-top, p
        //0,    1,            2,          3
        const okBtn = document.getElementById('attiva-scenari-btn');
        if (toggle.classList.contains('active')) {
            children[0].innerText = "Disabilitando questo scenario, disabiliterai l'antifurto.";
            children[2].innerText = "Disabilitare lo scenario?";
            children[3].classList.add('invisible');
            okBtn.innerText = "Disabilita";
        }
        else {
            children[0].innerText = "Attivando questo scenario, abiliterai l'antifurto.";
            if (scenari.default.scenarioAttivoToggle != null) {
                children[2].innerText = "Inoltre disabiliterai gli altri scenari attivi.";
                children[3].classList.remove('invisible');
            } else {
                children[2].innerText = "Continuare l'attivazione?";
                children[3].classList.add('invisible');
            }
            okBtn.innerText = "Attiva";
        }
        // launch the modal
        const modalLauncher = document.getElementById('launch-modal-' + scenarioID);
        modalLauncher.click();
    }, false);
}
/**
 * switches the toggle and updates the status of the light
 * @param {luceTemplate} luce the luce JSON luce (used to change its status)
 * @param {HTMLElement} toggle the toggle to move
 */
function luciToggleListener(luce,toggle) {
    toggle.addEventListener('click', event => {
        event.preventDefault();
        luce.stato = !toggle.classList.contains('active');
        // server send the toggle state
        try{
            Api.setLuciStato(luce,toggle,luciToggleCallback);
        } catch (error){
            console.log(error);
            showAlert("Errore",error.message,false);
        }
    }, false);
}

function luciToggleCallback(toggle){
    toggle.classList.toggle('active');
}

export { scenariToggleListener, luciToggleListener};