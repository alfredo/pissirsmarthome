'use strict';

function createRowDomain(domain) {
    return `
    <th scope="row">
        <h4 class="d-flex justify-content-center mt-2">
            <a href=${domain.stato != 0 ? '"/secured/home/"' : '"#"'}>${domain.nome}
            </a>
        </h4>
    </th>
    <td class="align-middle">
        <div class="switch-container no-box-sizing d-flex justify-content-center">
            <div class="toggle-button no-box-sizing ${domain.stato != 0 ? 'active' : ''}">
                <div class="inner-circle no-box-sizing"></div>
            </div>
        </div>
    </td>
    <td class="d-flex justify-content-center">
        ${domain.admin == true ? '<i class="fa-solid fa-trash fa-2x py-2">' : '<div class="trash-padding"></div>'}
    </td>`;
}

export {createRowDomain};