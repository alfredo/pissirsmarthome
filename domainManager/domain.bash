#!/bin/bash
javac -cp .:./org.json-1.0.0.v201011060100.jar:./fusionauth-jwt-5.2.1.jar:./sqlite-jdbc-3.36.0.3.jar:./jackson-databind-2.13.3.jar:./jackson-core-2.13.3.jar:./jackson-annotations-2.13.3.jar:./bcpkix-jdk13-167.jar:./bcprov-jdk13-167.jar -d bin src/code/* src/db/*
echo 'domain compiled'
cd bin
java -classpath .:../org.json-1.0.0.v201011060100.jar:../fusionauth-jwt-5.2.1.jar:../sqlite-jdbc-3.36.0.3.jar:../jackson-databind-2.13.3.jar:../jackson-core-2.13.3.jar:../jackson-annotations-2.13.3.jar:../bcpkix-jdk13-167.jar:../bcprov-jdk13-167.jar code.Domain $1
