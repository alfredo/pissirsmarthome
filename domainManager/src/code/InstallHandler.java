package code;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpsExchange;
import java.net.HttpURLConnection;

import db.DBC;
import db.Dominio;

public class InstallHandler implements HttpHandler {

	@Override
	public void handle(HttpExchange hex) throws IOException {
		HttpsExchange he = (HttpsExchange) hex;

		String requestMethod = he.getRequestMethod();
		// System.out.println(requestMethod);

		if (requestMethod.compareToIgnoreCase("options") == 0) {
			Helper.sendCors(he, 200);
			return;
		}
		if (requestMethod.compareToIgnoreCase("POST") != 0) {
			Helper.sendCors(he, 405);
			return;
		}
		String body = Helper.readBody(he.getRequestBody());
		// verifica user
		String user = Helper.checkTokenGetUser(he);
		if (user == null) {
			Helper.sendCors(he, 401);
			return;
		}
		JSONObject j = null;
		try {
			j = new JSONObject(body);

			String dm = j.getString("domain");
			Dominio d = DBC.getDom(dm);
			if (d != null) {
				Helper.sendCors(he, 403);
				return;
			}

		} catch (JSONException | SQLException e) {
			e.printStackTrace();
			return;
		}

		// inserisco i dati nel DB
		try {
			// qui leggo e parsifico i json nel body, inserisco tutti i campi nel db

			String domain = j.getString("domain");
			DBC.insertDom(domain);
			// questa chiamata serve ad assicurarsi che l'utente che ha effettuato la
			// chiamata sia anche amministratore del dominio
			JSONArray arrUsers = j.getJSONArray("users");
			DBC.insertAmministra(user, domain);
			int k = 0;
			boolean didUserSetHimself = false;
			while (k < arrUsers.length()) {
				JSONObject userObj = arrUsers.getJSONObject(k);
				k++;
				String usr = userObj.getString("user");
				if (user.equals(usr)) {//se l'utente e' colui che ha fatto la chiamata, l'ho gia' sistemato => passo al successivo
					didUserSetHimself = true;
					if(!userObj.getString("role").equals("A")){//the user didn't set himself as an admin, we have to correct that
						arrUsers.put(k-1, userObj.put("role","A"));//new JSONObject().put("user", usr).put("role", "A").put("passwd", userObj.getString("passwd")));
					}
					continue;
				}
				if (userObj.getString("role").equals("A")) {
					DBC.insertAmministra(usr, domain);
				} else if (userObj.getString("role").equals("U")) {
					DBC.insertUsa(usr, domain);
				} else
					System.err.println(userObj.toString());
			}
			if(!didUserSetHimself){//l'utente non si e' settato lui stesso come amministratore, lo faccio io
				arrUsers.put(new JSONObject().put("user", user).put("role", "A").put("passwd", user));
				j.put("users", arrUsers);
			}

			JSONArray arrServ = j.getJSONArray("services");// array con solo il nome dei servizi da installare
			// System.out.println("arrServ = "+arrServ);
			ArrayList<String> moduleHosts = new ArrayList<String>();
			for (int i = 0; i < arrServ.length(); i++) {
				String modul = arrServ.getString(i).replace("gpio","bbgpio").replace("arduino","AinDout_Arduino1wifi");
				moduleHosts.addAll(DBC.insertService(domain, modul));
			}
			// System.out.println("riga 105 moduleHost = " + moduleHosts);
			// divido gli host in base al loro module
			// Per ogni host e module, associo ad ogni module tutti i suoi host
			// moduleHosts puo' avere module0---host1 e module0---host2, hostsMap unisce i due host sullo stesso module
			HashMap<String, ArrayList<String>> hostsMap = new HashMap<String, ArrayList<String>>();
			for (String mh : moduleHosts) {
				String[] mhSplit = mh.split("---");
				String module = mhSplit[0];
				String host = mhSplit[1];
				ArrayList<String> hostsList;
				if (hostsMap.containsKey(module)) {
					hostsList = hostsMap.get(module);
				} else {
					hostsList = new ArrayList<String>();
					hostsMap.put(module, hostsList);
				}
				hostsList.add(host);
				// System.out.println("riga 120  modulo = "+ module + "  host = "+ host);
			}
			// imposta correttamente l'array dei servizi
			JSONArray arrServiziFinale = new JSONArray();
			for (int i = 0; i < arrServ.length(); i++) {
				JSONObject row = new JSONObject();
				String mod = arrServ.getString(i).replace("gpio","bbgpio").replace("arduino","AinDout_Arduino1wifi");

				// System.out.println("mod = "+mod + "     indice = "+i);
				ArrayList<String> ho = hostsMap.get(mod);
				// System.out.println("      riga 131 ho = "+ho);
				for (String h : ho) {
					row.put("host", h.replace("BB1","luci").replace("Arduino1","luci"));
					row.put("service", mod);
					row.put("uri", DBC.getURI(mod));
					arrServiziFinale.put(row);
				}
			}
			// System.out.println("riga 137 arrServiziFinale = "+arrServiziFinale);
			j.put("services", arrServiziFinale);

		} catch (SQLException | JSONException e) {
			e.printStackTrace();
		} catch ( Exception e2) {
			e2.printStackTrace();
		}
		// ora bisogna fare la chiamata al CloudApp, non prima!
		// effettuo chiamata a CloudAppManager

		// è una chiamata annidata nella risposta alla webapp
		// -richiesta REST da webApp a /install
		// -prendo da DB e poi chiamo CloudAppMng su /install
		// -attendo risposta da CloudAppMng e chiudo
		// -rispondo a webApp e chiudo
		// EZ

		HttpURLConnection con = Helper.sendMessageToCloudapp("install", j.toString());
		int status = con.getResponseCode();

		// FIXME serve avere anche il content?
		// String cloudappResponse = Helper.getResponseFromConnection(con);
		con.disconnect();

		// finita chiamata a CloudApp
		Helper.sendCors(he, status);
	}
}
