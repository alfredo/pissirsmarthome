package code;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URL;
import java.security.KeyStore;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.TrustManagerFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sun.net.httpserver.HttpsConfigurator;
import com.sun.net.httpserver.HttpsParameters;
import com.sun.net.httpserver.HttpsServer;

import db.DBC;

public class Domain {

	static public int port = 3001;

	public static void main(String[] args) throws IOException, JSONException {

		ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) Executors.newCachedThreadPool();

		HttpsServer server = HttpsServer.create(new InetSocketAddress(port), 0);// port gets set here

		// initialise the HTTPS server
		try {
			SSLContext sslContext = SSLContext.getInstance("TLS");

			// initialise the keystore
			char[] password = "miapasswd".toCharArray();
			KeyStore ks = KeyStore.getInstance("JKS");
			FileInputStream fis = new FileInputStream("../lig.keystore");
			ks.load(fis, password);

			// setup the key manager factory
			KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
			kmf.init(ks, password);

			// setup the trust manager factory
			TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
			tmf.init(ks);

			// setup the HTTPS context and parameters
			sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
			server.setHttpsConfigurator(new HttpsConfigurator(sslContext) {
				@Override
				public void configure(HttpsParameters params) {
					try {
						// initialise the SSL context
						SSLContext context = getSSLContext();
						SSLEngine engine = context.createSSLEngine();
						params.setNeedClientAuth(false);
						params.setCipherSuites(engine.getEnabledCipherSuites());
						params.setProtocols(engine.getEnabledProtocols());

						// Set the SSL parameters
						SSLParameters sslParameters = context.getSupportedSSLParameters();
						params.setSSLParameters(sslParameters);

					} catch (Exception ex) {
						System.out.println("Failed to create HTTPS port");
						ex.printStackTrace();
					}
				}
			});

			// chiamata per popolare moduli

			URL url = new URL("https://gitlab.di.unipmn.it/alfredo/iotlabgw.edu-al.unipmn.it/-/raw/main/index.json");
			HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

			Helper.setConnectionSettings(con, "GET");

			// leggo risposta
			// int status = con.getResponseCode(); //da problemi e non viene usato
			// controllare ToDo
			String content = Helper.getResponseFromConnection(con);
			con.disconnect();

			// manipolazione per ottenere i campi dei moduli
			riempiModuli(content);

			server.setExecutor(threadPoolExecutor);

			// API del server

			server.createContext("/install/", new InstallHandler());
			server.createContext("/start/", new StartHandler());
			server.createContext("/stop/", new StopHandler());
			server.createContext("/delete/", new DeleteHandler());
			server.createContext("/secured/domains/", new TokenHandler());
			server.createContext("/secured/services", new ServicesHandler());
			server.createContext("/secured/priviledges", new PriviledgesHandler());

			server.start();
			System.out.println("domain running on " + Helper.getSelfURL());

		} catch (Exception e) {
			System.out.println("Failed to create HTTPS server on " + Helper.getSelfURL());
			e.printStackTrace();
		}

	}

	private static void riempiModuli(String content) throws JSONException {

		JSONObject obj = new JSONObject(content);
		JSONArray jr = obj.getJSONArray("response");
		String[] s1 = new String[jr.length()];

		for (int i = 0; i < jr.length(); i++) {
			s1[i] = jr.getString(i);
		}
		for (int i = 0; i < s1.length; i++) {
			String remZip = s1[i].substring(0, s1[i].length() - 4);
			String remZipLowerCase = remZip.toLowerCase();
			String a = null;
			String b = null;
			if(remZipLowerCase.contains("-debian9-bb") && remZipLowerCase.contains("_arduino1wifi")){
				// find which one comes first
				int index1 = remZipLowerCase.indexOf("-debian9-bb");
				int index2 = remZipLowerCase.indexOf("_arduino1wifi");
				if(index1 < index2)
					a = remZip.substring(0, index1);
				else
					a = remZip.substring(0, index2);
				b = "debian9, Arduino1";
			}
			else if(remZipLowerCase.contains("-debian9-bb")){
				a = remZip.substring(0, remZipLowerCase.indexOf("-debian9-bb"));
				b = "debian9";
			}
			else if(remZipLowerCase.contains("_arduino1wifi")){
				a = remZip;
				b = "Arduino1";
			}
			else continue;

			try {
				// inserimento moduli debian9, Arduino1
				DBC.fillModules(a, b, s1[i]);
			} catch (SQLException | JSONException e) {
				e.printStackTrace();
			}
		}
	}
}