package utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.json.JSONException;
import org.json.JSONObject;

import scenari.Automa;
import scenari.Scenari;

public class Esecutore extends Thread {

	private final Automa automa;
	private final Scenari scenari;
	final String DB_FOLDER = Scenari.RES_FOLDER + "db-scenari/";
	private File scenarioAttivo = null;
	private final ArrayList<File> filesRead = new ArrayList<File>();
	private boolean isDonePublishing = false;
	private int retryNum = 0;
	private int filesNum = 0;
	private ArrayList<File> allScenariFiles;
	private UserChangeSync ucs;
	private boolean shouldFindNewScenario = true;
	private final String ESEC_CONF = Scenari.CONF_FOLDER+"esecutore.json";
	boolean chosenByUser = false;

	public Esecutore(Automa automa, Scenari scenari) throws IOException, JSONException {
		this.automa = automa;
		this.scenari = scenari;
		this.ucs = new UserChangeSync(this);
	}

	public UserChangeSync getUCS() {
		return ucs;
	}

	private void setup() throws IOException, JSONException, MqttException {
//		imposto lo scenario attivo
		JSONObject json = new JSONObject(Helper.leggiFile(ESEC_CONF));
		String scenarioAttivo = json.getString("scenario-attivo");
		if(scenarioAttivo.equals("none")){
			//se null e' disabilitato dall' utente
			this.scenarioAttivo = null;
			chosenByUser = true;
			isDonePublishing = true;
		} else {
			setScenarioAttivo(getScenarioFile(scenarioAttivo));
		}
	}

	public void run() {
//		qui ci va la business logic
		try {
			setup();
			while (true) {
				// if(shouldFindNewScenario) {
				if(automa.getAntifurto() && shouldFindNewScenario){
					LocalTime ora = LocalTime.now();
					boolean unreadStatus = automa.unreadStatus;
					if ((unreadStatus && automa.getAntifurto())// se leggo lo stato per la prima volta e l'antifurto e' attivo -> ciclo
							|| (!unreadStatus && automa.getAntifurto() && isDonePublishing))// se ho gia' letto lo stato e l'antifurto e' attivo e devo ancora pubblicare -> ciclo
						continue;
					// l'antifurto e' acceso
					FileReader fr = null;
					if(!ucs.getBoolean()) {
						if(scenarioAttivo == null && !isDonePublishing)
							fr = randomFileScenari();
						else if(scenarioAttivo != null)
							fr = new FileReader(scenarioAttivo);
					} else {
						try {
							while(ucs.getBoolean()) {
								wait();
							}
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						fr = new FileReader(scenarioAttivo);
					}
					if (fr == null)
						continue;
					BufferedReader br = new BufferedReader(fr);// selezioniamo un file contenente i comandi
					String line = br.readLine();// su ogni linea si trova un nuovo comando, in ordine di tempo dal meno recente al piu' recente
					while (line != null && line.trim().equals("")) {
						line = br.readLine();
					}
					LocalTime tempoCmd = LocalTime.now();
					boolean startedPublishing = false;
					boolean fileTooOld = false;
					while (line != null && (!isDonePublishing || ora.isBefore(tempoCmd)) && !ucs.getBoolean()) {
						ora = LocalTime.now();
						JSONObject jline = new JSONObject(line);
						String[] time = jline.getString("tempo").split("-");
						if (time.length != 3) {
							continue;
						}
						tempoCmd = LocalTime.of(Integer.valueOf(time[0]), Integer.valueOf(time[1]),
								Integer.valueOf(time[2]));// otteniamo [la data e] l'ora in cui e' stato inviato quel comando

						LocalTime tempoAttuale = LocalTime.now();
						try {
							long waitingTime = Helper.timeDifference(tempoAttuale, tempoCmd) * 1000;
							if (waitingTime > 0) {
								if (waitingTime > 1000) {
									Thread.sleep(waitingTime);
								}

								startedPublishing = true;
								String nomeLuce = jline.getString("luce");
								String cmd = jline.getString("cmd");
								scenari.sendMqttMessage(Scenari.getMqttTree("to/","gpio/" + nomeLuce),
										"{cmd:" + cmd + "}");
							} else fileTooOld = true;
						} catch (InterruptedException | MqttException e) {
							e.printStackTrace();
						}
						line = br.readLine();
						if (line != null)
							while (line.trim().equals("")) {
								line = br.readLine();
							}
						else
							isDonePublishing = true;
					}
	//				now line == null so I'm done reading the file
	//				TODO something about this
					br.close();
					fr.close();
					retryNum++;
					isDonePublishing = (fileTooOld && chosenByUser) || (startedPublishing && retryNum < filesNum);// if i havent published, I should retry
					scenarioAttivo = isDonePublishing ? null : scenarioAttivo;
				}
			}
		} catch (IOException | JSONException | MqttException e) {
			e.printStackTrace();
		}
	}

	synchronized void updateAvailableScenari() throws IOException {
		// Creating a File object for directory
		File directoryPath = new File(DB_FOLDER);
		// List of all files and directories
		final ArrayList<File> allScenariFiles = new ArrayList<File>(Arrays.asList(directoryPath.listFiles()));
		final ArrayList<File> filesList = new ArrayList<File>();
		for (File f : allScenariFiles) {
			if (f.toString().endsWith(".json"))
				filesList.add(f);
		}
		this.allScenariFiles = filesList;
	}

	private synchronized FileReader randomFileScenari() throws IOException, JSONException, MqttException {
		chosenByUser = false;
		updateAvailableScenari();
		filesNum = allScenariFiles.size();
		if (filesNum == 0)
			return null;
		if (filesNum == 1) {
			File f = allScenariFiles.get(0);
			setScenarioAttivo(f);
			return new FileReader(f);
		}
		File chosenFile = null;
		int retryNum = -1;
		do {
			chosenFile = allScenariFiles.get(((Double) (Math.random() * filesNum)).intValue());
			retryNum ++;
			if(retryNum > filesNum)
				return null;
		} while (chosenFile == null || filesRead.contains(chosenFile));
		setScenarioAttivo(chosenFile);
		return new FileReader(chosenFile);
	}

	synchronized void setScenarioAttivo(File f) throws IOException, JSONException, MqttException {
		if(!automa.getAntifurto())
			return;
		JSONObject k = new JSONObject();
		String oldScenario = getScenarioAttivo();
		String newScenario = getScenarioNome(f);
		k.put("scenario-attivo", newScenario);
		Helper.scriviFile(k.toString(), ESEC_CONF);
		scenarioAttivo = f;
		filesRead.add(f);
		if(oldScenario == null || (!chosenByUser && !oldScenario.equals("none") && !newScenario.equals("none"))){
			sendAutoActivation();
		}
	}

	public void sendAutoActivation() throws MqttException, JSONException{
		JSONObject k = new JSONObject();
		String scenario = getScenarioAttivo();
		if(scenario == null)
			return;
		k.put("scenario-attivo", scenario.split("---")[0]);
		scenari.sendMqttMessage(Scenari.getMqttTree("from", "scenari/attivazioneautomatica"), k.toString());
	}

	public String getScenarioAttivo() {
		return (isDonePublishing || scenarioAttivo == null) ? null : getScenarioNome(scenarioAttivo);
	}

	public String getDataScenarioAttivo() {
		String nome = getScenarioAttivo();
		return getScenarioData(nome);
	}

	public boolean isFileRenamed(String nomeFile) {
		//scenario-2022-07-13T12-14-54.591346.json
		//if the file doesn't match the regex, it's renamed
		return !nomeFile.matches("scenario-\\d{4}(-\\d{2}){2}T\\d{2}(-\\d{2}){2}.\\d{6}.json") && !nomeFile.matches("scenario-\\d{4}(-\\d{2}){2}T\\d{2}(-\\d{2}){2}.\\d{6}");
	}

	public boolean isFileRenamed(File f) {
		return isFileRenamed(f.toString());
	}

	public String getScenarioData(String scenario) {
		if (scenario == null)
			return null;
		scenario = scenario.endsWith(".json") ? removeExtension(scenario) : scenario;
		return isFileRenamed(scenario) ? scenario.split("---")[1] : scenario.substring(9, 19);
	}

	public String removeExtension(String nome) {
		return nome.endsWith(".json") ? nome.substring(0, nome.length()-5) : nome;
	}

	public String getScenarioNome(File f) {
		if(f == null)
			return "";
		String[] pathScenario = f.toString().split("/");
		return removeExtension(pathScenario[pathScenario.length - 1]);
	}

	public String removeData(String nome) {
		String[] split = nome.split("---");
		return split.length > 0 ? split[0] : nome;
	}

	public String getScenarioData(File f) {
		return getScenarioData(getScenarioNome(f));
	}

	public ArrayList<File> getAllScenariAvailable() throws IOException {
		updateAvailableScenari();
		return allScenariFiles;
	}

	void setLoopCondition(boolean loop) {
		shouldFindNewScenario = loop;
	}

	File getScenarioFile(String nome) throws IOException {
		File scen = null;
		if(allScenariFiles == null)
			updateAvailableScenari();
		for(File f : allScenariFiles) {
			String path = f.toString();
			if(path.contains(nome)) {
				scen = f;
				break;
			}
		}
		return scen;
	}

	public void releaseUserChoice(boolean isAntifurtoActive) throws MqttException, JSONException {
		chosenByUser = false;
		isDonePublishing = false;
		setLoopCondition(isAntifurtoActive);
		if(isAntifurtoActive)
			sendAutoActivation();
	}

}
