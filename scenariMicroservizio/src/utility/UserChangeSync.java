package utility;

import java.io.File;
import java.io.IOException;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.json.JSONException;
import org.json.JSONObject;

public class UserChangeSync {

	private boolean userWantsToChange = false;
	private Esecutore esec;

	public UserChangeSync(Esecutore esec) {
		this.esec = esec;
	}

	public synchronized boolean getBoolean() {
		return userWantsToChange;
	}

	public synchronized String requestToChange(JSONObject scenario) throws IOException, JSONException, MqttException {
		userWantsToChange = true;
		esec.setLoopCondition(true);
		esec.updateAvailableScenari();
		String scenarioNome = scenario.getString("nome");
		File scen = esec.getScenarioFile(scenarioNome);
		if(scen == null) {
			userWantsToChange = false;
			notifyAll();
			return null;
		}
		String scenarioAttivo = esec.getScenarioAttivo();
		String scenNome = esec.getScenarioNome(scen);
//		se sto attivando un nuovo scenario, sblocco l'esecutore; altrimenti lo lascio bloccato
		if(scenarioAttivo != null && scenarioAttivo.equals(scenNome)) {
			//in questo caso sto disabilitando lo scenario attivo
			esec.setLoopCondition(false);
			esec.setScenarioAttivo(new File(esec.DB_FOLDER+"none"));
		} else {
			//attivo lo scenario scelto dall'utente
			esec.setScenarioAttivo(scen);
		}
		userWantsToChange = false;
		esec.chosenByUser = true;
		notifyAll();
		return scenNome;
	}

}
