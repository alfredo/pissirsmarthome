package utility;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import scenari.Automa;
import scenari.Scenari;

public class SubscribeCallback implements MqttCallback{

	private Scenari scenari;
	private Esecutore esec;
	private Automa automa;
	/**
	 * nome dello scenario corrente, impostato quando si passa alla modalita' di apprendimento
	 */
	private String scenarioName;
	/**
	 * lista delle luci presenti e delle quali ascoltare i cambiamenti di stato durante la modalita' di apprendimento
	 */
	private ArrayList<String> luciOUT;

	public SubscribeCallback(Scenari scenari, Esecutore esec, Automa automa) {
		this.scenari = scenari;
		this.esec = esec;
		this.automa = automa;
	}

	@Override
	public void connectionLost(Throwable arg0) {
		final Date d = new Date();
		Date d2 = new Date();
		long time = Math.abs(d2.getTime()-d.getTime());
		boolean retry = true;
		while (retry && (time<600000)) {
			try {
				scenari.startClient(esec);
				retry = false;
			} catch (MqttException e) {
				d2 = new Date();
				time = Math.abs(d2.getTime()-d.getTime());
			}
		}
		if(time>=600000) {
			System.exit(1);
		}
	}


	@Override
	public void deliveryComplete(IMqttDeliveryToken arg0) {
		// Nessuna operazione
	}


	@Override
	public void messageArrived(String topic, MqttMessage message) throws MqttException, JSONException, IOException {
		if(topic.equals("to/all")) {
			sendRequested(Scenari.getMqttTree("from/","scenari/description"),false);
			return;
		}
		if(topic.equals(Scenari.getMqttTree("rpc/","scenari"))) {
			sendRequested(Scenari.getMqttTree("from/","scenari"),true);
			return;
		}

		JSONObject msgJson = new JSONObject(message.toString());
//		gli event sono 0 o 1; i trigger quando sono attivati/premuti inviano un 1 quando rilasciati, che e' l'unico elemento che mi interessa
		if(topic.startsWith("from/")){
			if(topic.equals(Scenari.getMqttTree("from/","gpio/"+scenari.getLearnTrigger()))) {
				if(!msgJson.has("event"))//se l'antifurto e' attivo, non posso attivare gli scenari
					return;
				int newStatus = msgJson.getInt("event");
				if(newStatus != 1)
					return;
				handleEvent(scenari.getLearnTrigger());
				return;
			}
			if(topic.equals(Scenari.getMqttTree("from/","gpio/"+scenari.getAttivaScenari()))) {
				if(!msgJson.has("event"))//se l'apprendimento e' attivo, non posso attivare l'antifurto
					return;
				int newStatus = msgJson.getInt("event");
				if(newStatus != 1)
					return;
				toggleAntifurtoLight();
				return;
			}
			if(topic.startsWith(Scenari.getMqttTree("from/","gpio/IN"))) {
				return;//non mi interessa altro da IN
			}
			if(topic.equals(Scenari.getMqttTree("from/","gpio/"+scenari.getLuceAntifurto()))) {
				if(msgJson.has("status")){
					int newStatus = msgJson.getInt("status");
					setAntifurtoStatus(newStatus == 1);
				} else if(msgJson.has("event")){
					contatoreMessaggiBB--;
					if(contatoreMessaggiBB < 0){
						contatoreMessaggiBB = 0;
						return;
					}
					handleEvent(scenari.getAttivaScenari());
				}
				return;
			}
			if(topic.startsWith(Scenari.getMqttTree("from/","gpio/OUT"))) {
				// mi interessa il messaggio <=> sto imparando e la luce ha cambiato di stato
				if(!automa.getLearn() || !msgJson.has("event"))//questo non mi interessa perche' faccio il sub solo a quelli che mi interessano || !luciOUT.contains(nomeOUT))
				return;
				if(scenarioName == null) {
					handleLearn(true);
				}
				int val = msgJson.getInt("event");
				// topic = from/gruppo2/luci/gpio/OUTx
				String luce = topic.substring(23);
				handleLearnData(luce,val);
				return;
			}
			return;
		}

		if(topic.startsWith(Scenari.getMqttTree("to", "scenari"))){
			if(topic.equals(Scenari.getMqttTree("to/","scenari"))) {
				if(!automa.getAntifurto() || !msgJson.has("evento") || !msgJson.has("nome"))
					return;
				changeActiveScenario(msgJson);
				return;
			}
			if(topic.equals(Scenari.getMqttTree("to/","scenari/salva"))) {
				if(!msgJson.has("nome"))//se non sono appena stato nello stato di registrazione, non serve andare avanti
					return;
				renameLastScenario(msgJson);
				return;
			}
			if(topic.equals(Scenari.getMqttTree("to","scenari/sensoreAntifurto"))){
				//serve se l'antifurto cambia sensore
				if(!msgJson.has("attiva-scenari"))
					return;
				changeSensor(msgJson.getString("attiva-scenari"));
				return;
			}
			if(topic.equals(Scenari.getMqttTree("to","scenari/luceAntifurto"))) {
				//serve se l'antifurto cambia sensore
				if(!msgJson.has("attiva-scenari"))
					return;
				changeLight(msgJson.getString("attiva-scenari"));
				return;
			}
			if(topic.equals(Scenari.getMqttTree("to","scenari/luci"))){
				if(!msgJson.has("luci"))
					return;
				subscribeToLights(msgJson.getJSONArray("luci"));
				return;
			}
			return;
		}
		if(topic.equals(Scenari.getMqttTree("conf", "scenari"))){
			changeConf(msgJson);
			return;
		}

		System.err.println("[Scenari] Impossibile: "+topic);
	}

	private void changeSensor(String newSensor) throws JSONException, IOException, MqttException {
		scenari.setAttivaScenari(newSensor);
		// query the luceAntifurto to set the correct status
		String req = "{request:status}";
		scenari.sendMqttMessage(Scenari.getMqttTree("to", "gpio/" + scenari.getLuceAntifurto()), req);
	}

	private void changeLight(String newLight) throws JSONException, IOException, MqttException {
		scenari.setLuceAntifurto(newLight);
	}

	private int contatoreMessaggiBB = 0;

	private void toggleAntifurtoLight() throws JSONException, IOException, MqttException {
		String luce = scenari.getLuceAntifurto();
		if(luce == null)
			return;
		String topic = Scenari.getMqttTree("to/","gpio/"+luce);
		int newStatus = automa.getAntifurto() ? 0 : 1;
		String msg = "{cmd:"+newStatus+"}";
		contatoreMessaggiBB++;
		scenari.sendMqttMessage(topic, msg);
	}

	private void handleEvent(String which) throws JSONException, MqttException, IOException{
		final int oldStatoAutoma = automa.getStato();
		final String cosa = which == scenari.getLearnTrigger() ? "learn" : "antifurto";
		final String cmd = automa.findPossibleCommand(cosa);
		if(cmd == null)
			return;
		automa.setStato(new JSONObject().put(cosa,cmd).toString());
		final int newStatoAutoma = automa.getStato();
		esec.setLoopCondition(automa.getAntifurto());
		if(Helper.compareText(cosa, "learn")) {
			final boolean val = Helper.compareText(cmd, "on");
			handleLearn(val);
		} else {
			esec.releaseUserChoice(newStatoAutoma == 2);
		}
		if(newStatoAutoma == oldStatoAutoma)
			return;
		switch(newStatoAutoma) {
			case 0://non sto registrando ne' l'antifurto e' attivo
				switch(oldStatoAutoma) {
					case 1://stavo registrando
						sendEventResponse(Scenari.getMqttTree("from/","scenari/learn"),true);
						break;
					case 2://l'antifurto era attivo
						sendEventResponse(Scenari.getMqttTree("from/","scenari/antifurto"),false);
						break;
				}
				break;
			case 1://sto registrando
				switch(oldStatoAutoma) {
					case 0://non stavo registrando ne' l'antifurto e' attivo
						sendEventResponse(Scenari.getMqttTree("from/","scenari/learn"),true);
						break;
					case 2://l'antifurto era attivo
						sendEventResponse(Scenari.getMqttTree("from/","scenari/learn"),true);
						sendEventResponse(Scenari.getMqttTree("from/","scenari/antifurto"),false);
						break;
				}
				break;
			case 2://l'antifurto e' attivo
				switch(oldStatoAutoma) {
					case 0://non stavo registrando ne' l'antifurto e' attivo
						sendEventResponse(Scenari.getMqttTree("from/","scenari/antifurto"),false);
						break;
					case 1://stavo registrando
						sendEventResponse(Scenari.getMqttTree("from/","scenari/learn"),true);
						sendEventResponse(Scenari.getMqttTree("from/","scenari/antifurto"),false);
						break;
				}
				break;
		}
	}

	private void sendEventResponse(String topic,boolean learn) throws JSONException, MqttException, IOException {
		JSONObject j = new JSONObject();
		j.put("event", learn ? automa.getLearn() : automa.getAntifurto());
		scenari.sendMqttMessage(topic, j.toString());
	}

	private void handleLearn(boolean shouldLearn) throws JSONException, IOException, MqttException{
		// passedThroughZero e' vero se l'automa e' passato dallo stato in cui l'antifurto/scenari sono attivi a quello in cui sta registrando (o viceversa)
		final Boolean passedThroughZero = automa.setStato(new JSONObject().put("learn",shouldLearn ? "on" : "off").toString());
		esec.setLoopCondition(automa.getAntifurto());
		if(passedThroughZero != null && !passedThroughZero){
			// devo assicurarmi di modificare lo stato della luce che gestisce l'antifurto
			String luce = scenari.getLuceAntifurto();
			if(luce == null)
				return;
			String topic = Scenari.getMqttTree("to/","gpio/"+luce);
			String msg = "{cmd:"+(shouldLearn ? 1 : 0)+"}";
			scenari.sendMqttMessage(topic, msg);
		}
		if(automa.getLearn()) {
			if(scenarioName == null) {
				scenarioName = "scenario-"+LocalDateTime.now().toString().replace(':', '-')+".json";
			}
		} else {
			scenarioName = null;
		}
	}

	private void handleLearnData(String luce, int val) throws MqttException, IOException, JSONException {
		JSONObject j = new JSONObject();
		LocalTime now = LocalTime.now();
		j.put("tempo", now.format(DateTimeFormatter.ofPattern("HH-mm-ss")));
		j.put("luce", luce);
		j.put("cmd", val);
		Helper.appendiFile(j, esec.DB_FOLDER+scenarioName,false);
	}

	private void changeActiveScenario(JSONObject scenario) throws IOException, JSONException, MqttException {
		String nuovoScenario = esec.getUCS().requestToChange(scenario);
		JSONObject j = new JSONObject();
		j.put("nome", nuovoScenario);
		scenari.sendMqttMessage(Scenari.getMqttTree("from/","/scenari/attiva"), j.toString());
	}

	private void sendRequested(String topic,boolean moreInfo) throws MqttException, JSONException, IOException {
		JSONObject j = new JSONObject();
		j.put("stato", automa.getStato());
		if(moreInfo) {
			j.put("learn", automa.getLearn());
			boolean antifurto = automa.getAntifurto();
			j.put("antifurto", antifurto);
			j.put("learn-trigger", scenari.getLearnTrigger());
			j.put("attiva-scenari", scenari.getAttivaScenari());
			j.put("luce-antifurto", scenari.getLuceAntifurto());
//			invio tutti gli scenari
			{
				ArrayList<File> allScenariFile = esec.getAllScenariAvailable();
				JSONArray arr = new JSONArray();
				for(File f : allScenariFile) {
					JSONObject scenarioData = new JSONObject();
					String scenNome = esec.getScenarioNome(f);
					scenarioData.put("nome", !esec.isFileRenamed(scenNome)? scenNome.substring(0,28) : esec.removeData(scenNome));
					scenarioData.put("data", esec.getScenarioData(f));
					arr.put(scenarioData);
				}
				j.put("scenari-disponibili", arr);
			}
//			invio lo scenario attivo
			if(antifurto) {
				String scenarioAttivo = esec.getScenarioAttivo();
				if(scenarioAttivo != null && !scenarioAttivo.equals("none")) {
					JSONObject scenarioValori = new JSONObject();
					scenarioValori.put("nome", !esec.isFileRenamed(scenarioAttivo) ? scenarioAttivo.substring(0,28) : esec.removeData(scenarioAttivo));
					scenarioValori.put("data", esec.getDataScenarioAttivo());
					j.put("scenario-attivo", scenarioValori);
				}
			}
		}
		scenari.sendMqttMessage(topic, j.toString());
	}

	private void renameLastScenario(JSONObject json) throws IOException, MqttException, JSONException {
		String nuovoNome = json.getString("nome");
		nuovoNome += nuovoNome.endsWith(".json") ? "" : ".json";
		ArrayList<File> available = esec.getAllScenariAvailable();
//		cerco l'ultimo scenario (sono nominati in base al giorno e all'ora, se non sono rinominati dall'utente)

		File max = available.get(0);
		String maxString = max.toString();
		for(File f : available) {
			String fString = f.toString();
			if(esec.isFileRenamed(f))//non mi interessano i file rinominati
				continue;
			if(esec.isFileRenamed(max) || fString.compareToIgnoreCase(maxString) > 0) {
				max = f;
				maxString = fString;
			}
		}
		String nome = esec.getScenarioNome(max);
		String[] path = max.toString().split("/");
		StringBuilder rename = new StringBuilder();
		for(int i = 0; i< path.length-1; i++)
			rename.append(path[i]).append('/');
		LocalDateTime now = LocalDateTime.now();
		String date = now.getYear()+"-"+(now.getMonthValue() < 10 ? "0"+now.getMonthValue() : now.getMonthValue())+"-"+(now.getDayOfMonth() < 10 ? "0"+now.getDayOfMonth() : now.getDayOfMonth());
		rename.append(esec.removeExtension(nuovoNome)).append("---").append(date).append(".json");

		boolean renamed = max.renameTo(new File(rename.toString()));

		JSONObject j = new JSONObject();
		String nuovoNomeSenzaEstensione = esec.removeData(esec.removeExtension(renamed ? nuovoNome : nome));
		j.put("nome", nuovoNomeSenzaEstensione);
		j.put("data", date);
		scenari.sendMqttMessage(Scenari.getMqttTree("from/","scenari/salva"), j.toString());
	}

	private void setAntifurtoStatus(boolean activateAntifurto) throws MqttException, JSONException, IOException {
		automa.setAntifurtoStatus(activateAntifurto);
		esec.setLoopCondition(automa.getAntifurto());
		sendRequested(Scenari.getMqttTree("from/","scenari/description"),false);
	}

	private void changeConf(JSONObject json) throws JSONException, IOException, MqttException {
		// json = { "learn-trigger" : IN4, "attiva-scenari" : in1, "luce-antifurto" : OUT3 }
		if(json.has("learn-trigger"))
			scenari.setLearnTrigger(json.getString("learn-trigger"));
		if(json.has("attiva-scenari"))
			scenari.setAttivaScenari(json.getString("attiva-scenari"));
		if(json.has("luce-antifurto"))
			scenari.setLuceAntifurto(json.getString("luce-antifurto"));
		// get all these values from scenari and send back
		String learnTrigger = scenari.getLearnTrigger();
		String attivaScenari = scenari.getAttivaScenari();
		String luceAntifurto = scenari.getLuceAntifurto();
		JSONObject j = new JSONObject();
		j.put("learnINbtn", learnTrigger)
			.put("antifurtoINbtn", attivaScenari)
			.put("outputAntifurtoStato", luceAntifurto);
		scenari.sendMqttMessage(Scenari.getMqttTree("from/","scenari"), j.toString());
	}

	private void subscribeToLights(JSONArray outArray) throws MqttException, JSONException {
		if(luciOUT == null)
			luciOUT = new ArrayList<String>();
		for(int i = 0; i < outArray.length(); i++) {
			String out = outArray.getString(i);
			scenari.mqttClient.subscribe(Scenari.getMqttTree("from/","gpio/"+out));
			luciOUT.add(out);
		}
	}
}
