package scenari;

import java.io.IOException;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import utility.Helper;

public class Automa {
//	0: learn = off, antifurto = off
//	1: learn = on, antifurto = off
//	2: learn = off, antifurto = on => scenari = on

	private Integer stato = null;
	private final JSONObject jsonObject;
	private boolean learn;
	private boolean antifurto;
	private final String DATABASE_PATH = Scenari.RES_FOLDER+"scenari.json";
	public boolean unreadStatus = false;
	public Integer ultimoStato = null;

	public Automa() throws JSONException, IOException {
		this.jsonObject = new JSONObject(Helper.leggiFile(DATABASE_PATH));
		setCollaterals(this.jsonObject.getInt("stato-attuale"));
	}

	public synchronized Boolean setStato(String command) throws JSONException, IOException {
		return setStato(command,false);
	}
	/**
	 * permette di cambiare lo stato dell'automa
	 * @param command JSONObject del tipo {"learn":"on","antifurto":"on"} o {"learn":"off","antifurto":"off"}
	 * @param fromDatabase se e' vero, significa che il comando e' stato ricevuto dalla database
	 * @return true se l'automa e' passato dallo stato neutrale 0, false altrimenti
	 * @throws JSONException
	 * @throws IOException
	 */
	public synchronized Boolean setStato(String command,boolean fromDatabase) throws JSONException, IOException {
		if (command == null || !Helper.checkJSON(command)
				|| (!command.contains("\"learn\":") && !command.contains("\"antifurto\":"))
				|| (!command.contains(":\"on\"") && !command.contains(":\"off\"")))
			return null;
		final String[] cmd = command.split(":");
		final String cosa = cmd[0].split("\"", 3)[1];
		final String come = cmd[1].split("\"", 3)[1];


		if (Helper.compareText(cosa, "learn")) {
			if (Helper.compareText(come, "on") && this.stato != 1) {
				setCollaterals(1,!fromDatabase);
				return this.ultimoStato == 0;
			} else if (Helper.compareText(come, "off") && this.stato != 0) {
				setCollaterals(0,!fromDatabase);
				return true;
			}
		}
		if (Helper.compareText(cosa, "antifurto")) {
			if (Helper.compareText(come, "on") && this.stato != 2) {
				setCollaterals(2,!fromDatabase);
				return this.ultimoStato == 0;
			} else if (Helper.compareText(come, "off") && this.stato != 0) {
				setCollaterals(0,!fromDatabase);
				return true;
			}
		}
		return null;
	}
	/**
	 * in base allo stato dell'automa e al parametro passato, restituisce quale sia il comando piu' probabile che si vuole eseguire in base allo stato dell'automa ed ai suoi possibili comandi
	 * @param which "learn" o "antifurto"
	 * @return "on" o "off"
	 * @throws JSONException
	 */
	public String findPossibleCommand(String which) throws JSONException{
		Boolean learn = null;
		if(!(learn=Helper.compareText(which, "learn")) && !Helper.compareText(which, "antifurto"))
			return null;
//		devo cercare nel jsonObject lo stato attuale e trovare la stringa corrispondente al which, poi discriminare in base a cio' che mi permette di cambiare stato
		@SuppressWarnings("unchecked")
		Iterator<String> keys = jsonObject.keys();
		while(keys.hasNext()) {
			String key = keys.next();
			try {
				JSONArray array = jsonObject.getJSONArray(key);
				for(int i =0; i<array.length(); i++) {
					JSONObject a = array.getJSONObject(i);
					int stato = a.getInt("stato");
					if(stato != this.stato)
						continue;
					if(learn) {
						int learnOff = a.getInt("learn-off");
						if(learnOff != stato)
							return "off";
						int learnOn = a.getInt("learn-on");
						if(learnOn != stato)
							return "on";
					} else {
						int antifurtoOff = a.getInt("antifurto-off");
						if(antifurtoOff != stato)
							return "off";
						int antifurtoOn = a.getInt("antifurto-on");
						if(antifurtoOn != stato)
							return "on";
					}
				}
			} catch (JSONException e) {
				if(!e.getMessage().contains("stato-attuale"))
					e.printStackTrace();
			}
//			if(riga.getInt("stato")==statoAttuale) {
//				this.statoAttuale = riga.getInt("cmd"+newStatoInterruttore);
//				aggiornaInterruttoreESuono();
//			}
		}
		return null;
	}

	public synchronized void setAntifurtoStatus(boolean status) throws JSONException, IOException {
		if(status == this.antifurto)
			return;
		if(status)
			setCollaterals(2,true);
		else
			setCollaterals(0,true);
	}

	private synchronized void setCollaterals(int stato) throws JSONException, IOException {
		setCollaterals(stato,false);
	}

	private synchronized void setCollaterals(int stato,boolean write) throws JSONException, IOException {
		this.ultimoStato = this.stato != null ? this.stato : null;
		switch (stato) {
			case 0: {
				this.stato = 0;
				this.learn = false;
				this.antifurto = false;
				break;
			}
			case 1: {
				this.stato = 1;
				this.learn = true;
				this.antifurto = false;
				break;
			}
			case 2: {
				this.stato = 2;
				this.learn = false;
				this.antifurto = true;
				break;
			}
		}
		unreadStatus = true;

		if(write) {
			this.jsonObject.put("stato-attuale", this.stato);
			Helper.scriviFile(jsonObject,DATABASE_PATH);
		}
	}

	public synchronized int getStato() {
		unreadStatus = false;
		return this.stato;
	}

	public synchronized boolean getLearn() {
		unreadStatus = false;
		return this.learn;
	}

	public synchronized boolean getAntifurto() {
		unreadStatus = false;
		return this.antifurto;
	}

}
