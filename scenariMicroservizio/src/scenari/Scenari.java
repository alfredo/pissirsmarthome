package scenari;


import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.Security;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;
import java.util.Date;

import java.security.KeyPair;
import java.security.KeyStore;
import java.security.Security;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMDecryptorProvider;
import org.bouncycastle.openssl.PEMEncryptedKeyPair;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.openssl.jcajce.JcePEMDecryptorProviderBuilder;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttTopic;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONException;
import org.json.JSONObject;

import utility.Esecutore;
import utility.Helper;
import utility.SubscribeCallback;

public class Scenari {

	private String learnTrigger;
	private String attivaScenari;
	private String luceAntifurto;
	private static ArrayList<String> topicsSub;
	private Automa automa;
	private static String mqttTree;
	private static final String BASE_DIR = "/home/debian/CONFIG/scenari/";
	public static final String RES_FOLDER = BASE_DIR + "res/";
	public static final String CONF_FOLDER = RES_FOLDER+"CONF/";
	private final String FILE_CONF = CONF_FOLDER+"conf.json";
	private final String FILE_ZONA = CONF_FOLDER+"zona.json";

	public MqttClient mqttClient;

	private String brokerUrl;

	public Scenari(Automa automa) throws JSONException, IOException, MqttException {
		this.automa = automa;

		JSONObject config = new JSONObject(Helper.leggiFile(FILE_CONF));
		brokerUrl = config.getString("protocol") + "://" + config.getString("broker") + ":" + config.getInt("port");
		String mqttDomain = config.getString("mqttDomain");
		String mqttSubdomain = config.getString("mqttSubdomain");
		mqttTree = mqttDomain + "/" + mqttSubdomain + "/";

		topicsSub = new ArrayList<String>();
		topicsSub.add("to/all"); //mandare la mia descrizione json
		topicsSub.add(getMqttTree("rpc/","scenari")); //mandare lo stato attuale

		// inutile aggiungere i topic, perche subbo gia' tutto gpio/#
		config = new JSONObject(Helper.leggiFile(FILE_ZONA));
		learnTrigger = config.getString("learn-trigger");
		attivaScenari = config.getString("attiva-scenari");
		luceAntifurto = config.getString("luce-antifurto");
		topicsSub.add(getMqttTree("from/","gpio/"+learnTrigger));
		topicsSub.add(getMqttTree("from/","gpio/"+attivaScenari));
		topicsSub.add(getMqttTree("from/","gpio/"+luceAntifurto));

		topicsSub.add(getMqttTree("to/","scenari/#"));//{"evento":0,"nome":nome_scenario} per attivare/disattivare uno scenario; risposta su from/tree/scenari/attiva con {"nome":nome_scenario}
		topicsSub.add(getMqttTree("conf/","scenari"));//per admin

		String clientId = Long.toString(new Date().getTime()) + "-scenari"; // unique client id
		this.mqttClient = new MqttClient(brokerUrl, clientId, new MemoryPersistence());
		//memory persistence serve per non avere errori in console
		// https://github.com/eclipse/paho.mqtt.java/issues/794
	}

	public void startClient(Esecutore esec) throws MqttException {
		String caFilePath = "";
		String clientCrtFilePath = "";
		String clientKeyFilePath = "";

		MqttConnectOptions options = new MqttConnectOptions();
		options.setCleanSession(false);
		if(brokerUrl.contains("luci.local")) {  // devo connettermi al mosquitto della beaglebone
			caFilePath = BASE_DIR + "certificates/beaglebone/caGruppo2.crt";
			clientCrtFilePath = BASE_DIR + "certificates/beaglebone/clientGruppo2.crt";
			clientKeyFilePath = BASE_DIR + "certificates/beaglebone/clientGruppo2.key";

			options.setUserName("gruppo2");
			options.setPassword("funziona".toCharArray());
		}
		else {
			System.out.println("Unknown broken url " + brokerUrl);
			System.exit(1);
		}

		options.setMqttVersion(MqttConnectOptions.MQTT_VERSION_3_1);

		SSLSocketFactory socketFactory;
		try {
			socketFactory = getSocketFactory(caFilePath, clientCrtFilePath, clientKeyFilePath, "");
			options.setSocketFactory(socketFactory);
		} catch (Exception e) {
			e.printStackTrace();
		}


		mqttClient.setCallback(new SubscribeCallback(this, esec, automa));

		mqttClient.connect(options);

		for(String t: topicsSub)
			mqttClient.subscribe(t);

		// query the antifurto microservice to set the same luceAntifurto
		String req = "{\"request\":\"out\"}";
		sendMqttMessage(Scenari.getMqttTree("to", "antifurto/luceAntifurto"), req);
		// query the luci microservice to get all the OUT for the lights
		sendMqttMessage(Scenari.getMqttTree("to", "luci/outs"), req);
	}

	public void sendMqttMessage(String topic, String msg) throws MqttException {
		final MqttTopic msgTopic = mqttClient.getTopic(topic);
		msgTopic.publish(new MqttMessage(msg.getBytes()));
	}

	public static String getMqttTree(String prefix,String suffix) {
		return (prefix.endsWith("/") ? prefix : prefix+"/") + mqttTree + (suffix.startsWith("/") ? suffix.substring(1) : suffix);
	}

	public static void main(String args[]) throws JSONException, IOException, MqttException {
		System.out.println("scenari started");
		startSystem();
	}


	private static void startSystem() throws JSONException, IOException, MqttException {
		Automa automa = new Automa();
		Scenari scenari = new Scenari(automa);
		Esecutore esec = new Esecutore(automa,scenari);

		scenari.startClient(esec);
		esec.start();
	}

	public String getLearnTrigger() {
		return learnTrigger;
	}

	public String getAttivaScenari() {
		return attivaScenari;
	}

	public void setAttivaScenari(String attivaScenari) throws JSONException, IOException, MqttException {
		if(!attivaScenari.matches("(IN|in)[0-7]"))
			throw new IllegalArgumentException("attivaScenari non corretto");
		// write attivaScenari to the file zona
		JSONObject config = new JSONObject(Helper.leggiFile(FILE_ZONA));
		config.put("attiva-scenari", attivaScenari);
		Helper.scriviFile(config, FILE_ZONA);
		mqttClient.unsubscribe(Scenari.getMqttTree("from/","/gpio/" + this.attivaScenari));
		mqttClient.subscribe(Scenari.getMqttTree("from/","gpio/"+attivaScenari));
		this.attivaScenari = attivaScenari;
	}

	public String getLuceAntifurto() {
		return luceAntifurto;
	}

	public void setLuceAntifurto(String luceAntifurto) throws JSONException, IOException, MqttException{
		if(!luceAntifurto.matches("(OUT|out)[0-7]"))
			throw new IllegalArgumentException("luceAntifurto non corretta");
		// write luceAntifurto to the file zona
		JSONObject config = new JSONObject(Helper.leggiFile(FILE_ZONA));
		config.put("luce-antifurto", luceAntifurto);
		Helper.scriviFile(config, FILE_ZONA);
		mqttClient.unsubscribe(Scenari.getMqttTree("from/","/gpio/" + this.luceAntifurto));
		mqttClient.subscribe(Scenari.getMqttTree("from/","gpio/"+luceAntifurto));
		this.luceAntifurto = luceAntifurto;
	}

	public void setLearnTrigger(String learnTrigger) throws JSONException, IOException, MqttException{
		if(!learnTrigger.matches("(IN|in)[0-7]"))
			throw new IllegalArgumentException("learnTrigger non corretto");
		// write learnTrigger to the file zona
		JSONObject config = new JSONObject(Helper.leggiFile(FILE_ZONA));
		config.put("learn-trigger", learnTrigger);
		Helper.scriviFile(config, FILE_ZONA);
		mqttClient.unsubscribe(Scenari.getMqttTree("from/","/gpio/" + this.learnTrigger));
		mqttClient.subscribe(Scenari.getMqttTree("from/","gpio/"+learnTrigger));
		this.learnTrigger = learnTrigger;
	}

	private static SSLSocketFactory getSocketFactory(final String caCrtFile,
			final String crtFile, final String keyFile, final String password)
			throws Exception {
		Security.addProvider(new BouncyCastleProvider());

		// load CA certificate
		X509Certificate caCert = null;

		FileInputStream fis = new FileInputStream(caCrtFile);
		BufferedInputStream bis = new BufferedInputStream(fis);
		CertificateFactory cf = CertificateFactory.getInstance("X.509");

		while (bis.available() > 0) {
			caCert = (X509Certificate) cf.generateCertificate(bis);
		}

		// load client certificate
		bis = new BufferedInputStream(new FileInputStream(crtFile));
		X509Certificate cert = null;
		while (bis.available() > 0) {
			cert = (X509Certificate) cf.generateCertificate(bis);
		}

		// load client private key
		PEMParser pemParser = new PEMParser(new FileReader(keyFile));
		Object object = pemParser.readObject();
		PEMDecryptorProvider decProv = new JcePEMDecryptorProviderBuilder()
				.build(password.toCharArray());
		JcaPEMKeyConverter converter = new JcaPEMKeyConverter()
				.setProvider("BC");
		KeyPair key;
		if (object instanceof PEMEncryptedKeyPair) {
			//Encrypted key - we will use provided password
			key = converter.getKeyPair(((PEMEncryptedKeyPair) object)
					.decryptKeyPair(decProv));
		} else {
			//Unencrypted key - no password needed
			key = converter.getKeyPair((PEMKeyPair) object);
		}
		pemParser.close();

		// CA certificate is used to authenticate server
		KeyStore caKs = KeyStore.getInstance(KeyStore.getDefaultType());
		caKs.load(null, null);
		caKs.setCertificateEntry("ca-certificate", caCert);
		TrustManagerFactory tmf = TrustManagerFactory.getInstance("X509");
		tmf.init(caKs);

		// client key and certificates are sent to server so it can authenticate
		// us
		KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
		ks.load(null, null);
		ks.setCertificateEntry("certificate", cert);
		ks.setKeyEntry("private-key", key.getPrivate(), password.toCharArray(),
				new java.security.cert.Certificate[] { cert });
		KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory
				.getDefaultAlgorithm());
		kmf.init(ks, password.toCharArray());

		// finally, create SSL socket factory
		SSLContext context = SSLContext.getInstance("TLSv1.2");
		context.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

		return context.getSocketFactory();
	}
}
