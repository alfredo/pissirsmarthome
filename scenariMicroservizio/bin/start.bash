#!/bin/bash
status="OFF"
running=$(/usr/bin/pgrep -f scenari.Scenari)
if [ -f "/home/debian/CONFIG/scenari/STATUS" ]; then status=$(cat /home/debian/CONFIG/scenari/STATUS); fi
if [ ! -z "$running" ]; then status="OFF"; fi
if [ $status == "ON" ]; then (/usr/local/jdk1.8.0_321/bin/java -classpath /home/debian/CONFIG/scenari/bin:/home/debian/CONFIG/scenari/org.eclipse.paho.client.mqttv3_1.2.5.jar:/home/debian/CONFIG/scenari/org.json-1.0.0.v201011060100.jar:/home/debian/CONFIG/scenari/bcprov-jdk13-167.jar:/home/debian/CONFIG/scenari/bcpkix-jdk13-167.jar scenari.Scenari &>/dev/null&); fi
