#!/bin/bash

#keycloak.bash
#scenari.bash
#antifurto.bash
#server.bash
#luci.bash
#cloudappmanager.bash
#domain.bash

for i in `ps -ef | grep "org.json-1.0.0.v201011060100.jar" | grep -v "grep" | awk '{print $2}'`
do
	kill -15 $i
done

for i in `ps -ef | grep "start-dev" | grep -v "grep" | awk '{print $2}'`
do
	kill -15 $i
done

echo '
tutto chiuso'
