# Introduzione
Il progetto presentato dal gruppo2 si occupa di fornire ad un utente funzionalità di domotica in una LAN, controllabili anche dall'esterno.

L’obiettivo del nostro progetto è realizzare:
- un sistema di controllo delle luci;
- un sistema di antifurto;
- scenari di accensione e spegnimento automatico delle luci 

L’accesso all’intero sistema è controllato tramite autenticazione con OAUTH2.0.

# 1. Specifiche funzionali
## 1.1. Controllo tramite sensori ed attuatori
Il sistema nella LAN è costituito da una BeagleBone (BB) e un Arduino. Sulla BB sono presenti pulsanti e led, mentre sull'Arduino è collegato un sensore di luminosità.

I pulsanti simulano gli interruttori e i sensori di movimento.

L’accensione e lo spegnimento dell’antifurto è controllato da un interruttore presente sulla BB. I sensori di movimento consentono il controllo dell'allarme dell'antifurto. Due led permettono di capire se l’antifurto è spento, acceso o sta suonando. 

Ciascuna luce configurata nel sistema usa un interruttore e un led. Il sensore di luminosità insieme a un sensore di movimento permettono l’accensione automatica delle luci al passaggio delle persone durante la notte.

È possibile avviare e interrompere la modalità di apprendimento negli scenari di accensione e spegnimento delle luci premendo un apposito interruttore. 

## 1.2. Accessibilità via internet
Il sistema, installato in una rete privata, è accessibile via internet. 
L’autenticazione è basata sul protocollo OAUTH2.0, implementato tramite Keycloak. 
La sicurezza è garantita dall’uso dei protocolli HTTPS e WSS. 
Dopo aver effettuato l’accesso al sistema, l’utente può controllare e gestire i vari microservizi, installati nella LAN, navigando nella Webapp. 

# 2. Analisi della tecnologia
## 2.1. Installazione del sistema in reti private
Il sistema deve essere installato in reti private senza dover intervenire con configurazioni che riguardino l’apparato di rete.

L'installazione del sistema avviene senza configurazioni particolari: utilizzando Mosquitto con la configurazione da noi impostata, è sufficiente collegare il sistema alla rete internet per avere le funzionalità richieste disponibili.
## 2.2. Accessibilità da rete pubblica e da rete privata
Il sistema deve essere accessibile dalla rete pubblica senza differenze rispetto all’accesso da rete interna.

L’uso di brokers Mosquitto (in LAN e in Cloud) connessi in bridging garantisce l’accessibilità del sistema sia all'interno della LAN sia al suo esterno. 

La Webapp, connettendosi a uno di questi broker, permette all'utente di controllare tutti i suoi servizi. 

In ogni caso, per accedere al servizio l'utente deve autenticarsi.
Il login è gestito grazie a Keycloak che autorizza l'accesso alla Webapp. 

## 2.3. Caratteristiche del traffico dati da sostenere 
La quantità di traffico da gestire è appropriata. Le uniche 2 fonti di produzione di messaggi del sistema sono il servizio della BBGPIO, che pubblica a tempo costante di 10s; ed il microservizio dell'antifurto, che pubblica ogni 30s.
La componente umana non è precisamente quantificabile, ma proporzionale nella quantità di messaggi prodotti rispetto al numero di lampadine, sensori e interruttori utilizzati da ogni dominio in un determinato momento. 

## 2.4. Vincoli in tempo reale
Essendo un sistema di domotica, è necessario che i comandi impartiti dall'utente vengano attuati una ed una sola volta ed in tempo utile, in modo tale da non inficiare l'esperienza dell'utente che deve risultare al pari o migliore dell'esperienza analogica.

## 2.5. Tecniche viste a lezione con cui soddisfare i requisiti
Le tecniche viste a lezione sono:
- RPC e API REST
- Message brokers e implementazione di un'architettura event-driven
# 3. Approccio tecnologico
La scelta effettuata è stata quella di usare un message broker. 
## 3.1. Vantaggi dell'uso di un message broker 
Il message broker risulta fondamentale nello sviluppo di un'applicazione formata da vari microservizi, permettendone la comunicazione. Inoltre facilita la separazione dei problemi della comunicazione dalla business logic del servizio.

L'utilizzo di Mosquitto assicura il recapito di ogni messaggio nel numero a noi necessario (impostando il QoS a 2 si riceve uno e un solo messaggio).
La scelta di utilizzare il protocollo MQTT è dovuta al fatto che è un protocollo molto semplice e leggero, che permette di inviare messaggi in modo veloce e sicuro. Questo protocollo permette di evitare l'invio di messaggi a tutti i microservizi: solo quelli che sono effettivamente interessati al messaggio lo riceveranno.

La centralità del message broker permette la sincronizzazione di tutti i dispositivi ad esso connessi in tempo reale. A differenza dell'utilizzo di RPC, questa soluzione evita lo spreco di risorse causato dal polling dei dispositivi. 

# 4. Architettura del software

![Architettura](./architettura.jpg)

## 4.1. Organizzazione del software (evidenziandone i moduli)
Nel sistema troviamo:
- l'istanza di Keycloak per l'autenticazione,
- il Webserver,
- il Domain Manager (DM),
- il Cloud App Manager (CAM),
- i broker connessi in bridging,
- la LAN domestica.

L'istanza di Keycloak si occupa di gestire l'autenticazione degli utenti. Se l'autenticazione ha successo, rilascia un Token JWT (JSON Web Token), utilizzato ogni volta che l'utente accede alle risorse.

Il client che esegue nel browser effettuerà chiamate al Webserver per ottenere risorse, potendo così mostrare le pagine all'utente.

Il DM espone un'interfaccia REST, consentendo all'utente che naviga nel browser di effettuare chiamate per installare, avviare, fermare o cancellare domini.

Il CAM, a seguito di chiamate REST da parte del DM, pubblica opportuni messaggi sul Mosquitto presente sul cloud, connesso in bridging con il Mosquitto presente sulla BB. 

Nella LAN è presente la porzione di sistema che si occupa di comandare le luci, l'antifurto e gli scenari.
Essa è composta da una BB e da un Arduino. 
Sulla BB è in esecuzione un broker Mosquitto, a cui si collegano i vari microservizi e il programma in esecuzione sull'Arduino. 

Le luci, gli scenari e l'antifurto sono installati sulla BB. 
Il Webserver fornisce la Webapp. L'utente, navigando nella Webapp, può controllare e gestire i servizi installati nella LAN. 


## 4.2. Distribuzione delle funzionalità tra i moduli, attività e loro interazione

### 4.2.1. Keycloak
#### Responsabile: Elisa Giglio
L'autenticazione è gestita con Keycloak. Per ottenere il "grant" di accesso è stata utilizzata la sequenza "Authorization Grant con PKCE". 

La pagina iniziale si trova all'URL https://localhost:3000/
Per rispondere alla richiesta GET, il Webserver legge la pagina redirect.html e imposta in essa $DOMAIN, $REALM, $MY_REDIRECT_URI in base a quanto scritto nel file kecloak.json (contentente i parametri di configurazione di keycloak) e nel file params.json (contenente ulteriori informazioni di keycloak).
La pagina fornita contiene un solo link (non visibile all'utente). Tramite JavaScript, il client genera un segreto (Code Verifier) 
e ne calcola l'hash usando SHA256 (Code Challenge).
Si effettua poi una redirezione della pagina al link specificato, ovvero http://localhost:8080/realms/test00/protocol/openid-connect/auth. In questa richiesta GET sono presenti i seguenti query parameters: 
- response_type, settato a 'code'; 
- code_challenge;
- code_challenge_method, settato a 'S256';
- client_id, settato a 'myclient';
- redirect_uri, settato a 'https://localhost:3000/secured' (questo è l'URL registrato presso l'authorization server al quale il browser verrà rediretto nel caso in cui il login vada a buon fine);
- scope, settato a 'openid';
- nonce; 
- response_mode, settato a 'fragment';
- state.

Viene quindi mostrata la pagina di login: l'utente inserisce username e password. 
Se l'autenticazione ha successo, l'authorization server ridirige il client all'URL fornitogli alla precedente richiesta GET (ovvero https://localhost:3000/secured). Nell'URL è presente anche 'state' (il cui valore sarà lo stesso inviato nella prima richiesta GET) e 'code' (valorizzato con l'authorization code appena generato dall'authorization server).
Il browser ottiene dal Webserver la pagina domains.html (che è la prima pagina dell'applicazione) e provvede a inviare una richiesta POST all'URL http://localhost:8080/realms/test00/protocol/openid-connect/token per ottenere il token. Nel body della richiesta sono specificati i seguenti parametri:
- grant_type, settato a 'authorization_code';
- client_id, settato a 'myclient';
- code_verifier
- code, valorizzato con l'authentication code ricevuto 
- redirect_uri, settato come nella richiesta GET. 

A seguito di questa richiesta, l'authorization server rilascia un token e un corrispondente refresh_token (con il quale richiedere un nuovo token).

Periodicamente, prima che scada il token, viene fatta la richiesta di un nuovo token utilizzando il refresh token. In particolare si effettua una POST all'URL http://localhost:8080/realms/test00/protocol/openid-connect/token. Nel body della richiesta sono specificati i seguenti parametri:
- grant_type, settato a 'refresh_token';
- client_id, settato a 'myclient';
- refresh_token.

L'accesso alle risorse è consentito solo se si presenta il token. In ogni richiesta inviata al DM è presente l'header 'Authorization' cui è associato il valore 'Bearer ' seguito dal token corrente.

Sia nella pagina contenente la lista dei domini cui l'utente può accedere sia nella pagina di visualizzazione e gestione dei microservizi è presente un bottone per effettuare il logout. 
Cliccandolo viene effettuata una richiesta GET all'URL http://localhost:8080/realms/test00/protocol/openid-connect/logout. Nell'URL sono presenti i seguenti query parameters: 
- id_token_hint
- post_logout_redirect_uri, settato a 'https://localhost:3000/'

Specificando il parametro id_token_hint, il logout viene effettutato senza che l'utente debba darne una ulteriore conferma.
Il parametro post_logout_redirect_uri consente di effettuare il redirect automatico a https://localhost:3000/ dopo il logout. 

### 4.2.2. Webserver e Webapp 
#### Responsabile: Alfredo Chissotti (Webserver e seconda pagina della Webapp), Elisa Giglio (prima pagina della Webapp)
Il Webserver, implementato come server HTTPS concorrente, fornisce la Webapp al client grazie all'uso di API REST. 

La Webapp comunica con il server di autenticazione (Keycloak) ed effettua richieste HTTPS al DM. Nel momento il cui l'utente seleziona un dominio, la Webapp si connette al message broker, mediante il protocollo WSS, consentendo così il controllo e la gestione di tutti i servizi presenti. 

### 4.2.3. Domain Manager 
#### Responsabile: Alessandro Carbonelli
Il DM è un microservizio con funzionalità da server REST ed interagisce con il DB dell'intero sistema.
Si occupa di soddisfare le richieste HTTPS provenienti dalla Webapp, controllandone la validità e la consistenza col DB e inoltrarle al CAM richiamando degli URI esposti da quest'ultimo.
Dopo aver verificato l'autorizzazione tramite il Token presente nell'header della richiesta della Webapp, il DM interagisce con il DB ed effettua la chiamata al CAM.
Terminate queste due interazioni, viene servita la risposta alla Webapp.

### 4.2.4. BeagleBone
- Luci

Responsabile: Alessandro Carbonelli

Microservizio che opera in LAN che si occupa di gestire l'accensione e lo spegnimento delle lampadine sia tramite interruttore sia tramite sensori di movimento.
I comandi e gli stati delle lampadine vengono letti e scritti su due topic distinti del broker Mosquitto presente sulla BB.
La persistenza dello stato del microservizio è mantenuta in locale su un file testuale in formato JSON, che viene aggiornato per le seguenti variazioni nel microservizio: accensione o spegnimento di una lampadina, aggiunta di una nuova lampadina e modifica del sensore di movimento.


- Antifurto

Responsabile: Elisa Giglio

Permette la gestione di un sistema di antifurto. Un pulsante consente di attivare / disattivare l'antifurto. I sensori di movimento sono simulati mediante pulsanti sulla BB. A ciascun sensore di movimento corrisponde un valore delta. 
In caso di antifurto impostato, l'allarme scatta nel momento in cui si supera un certo valore soglia. Questo meccanismo è stato pensato per cercare di evitare falsi allarmi. 
Due led consentono all'utente di conoscere lo stato dell'antifurto, ovvero se esso è impostato e se sta suonando. 
L'intero sistema è configurabile dall'utente: è possibile cambiare il pulsante di accensione/spegnimento dell'antifurto, la soglia oltre la quale scatta l'allarme e i due led. E' anche possibile aggiungere sensori di movimento. 
La persistenza è realizzata mediante la scrittura e la lettura di file JSON. Si mantiene traccia dello stato dell'antifurto, del valore raggiunto, del momento in cui è scattato l'allarme, della soglia, dei sensori di movimento presenti (con i corrispondenti valori delta), dell'interruttore di accensione e spegnimento dell'antifurto e dei due led. 


- Scenari

Responsabile: Alfredo Chissotti

Gestisce la deterrenza delle intrusioni riproponendo uno schema di accensione e spegnimento delle luci quando l'utente attiva l'antifurto.
Lo schema registra lo stato delle luci dopo che l'utente ha attivato la modalità di apprendimento; alla sua disattivazione, la registrazione è immediatamente disponibile poiché viene salvata su un file come sequenza di JSON.
La persistenza è garantita grazie all'uso di tre file che vengono aggiornati in momenti diversi: uno ricorda quale scenario è attivo; il secondo permette il funzionamento dell'automa e memorizza lo stato attuale; il terzo è usato per salvare gli interruttori e la luce attualmente in uso.


### 4.2.5. Arduino
Questo componente della LAN scrive sul broker Mosquitto della BB i valori del sensore di luminosità. Se il valore supera 200, si inibisce l'accensione comandata dai sensori di movimento delle luci.


### 4.2.6 Brokers Mosquitto
#### Responsabile: Elisa Giglio
Il Mosquitto che gira sul computer ha 2 listener:
- uno in ascolto sulla porta 8883 che usa il protocollo MQTT,
- uno in ascolto sulla porta 9001 che usa il protocollo websockets. 

Entrambe le porte sono protette con TLS. 

In particolare, per il listener sulla porta 8883 sono state create una coppia di chiavi e un certificato (caGruppo2.crt) di una Certification Authority (CA) fittizia 'gruppo2Cert'. Dopodichè è stata creata una chiave per il server (serverGruppo2.key), una richiesta (serverGruppo2.csr) per ottenere la firma da parte della CA 'gruppo2Cert' e infine il certificato del server (serverGruppo2.crt) firmato dalla CA 'gruppo2Cert'. Inoltre, poichè 'require_certificate' è settato a 'true', un client che vuole connettersi su questa porta deve inviare il suo certificato. 

caWebServer.crt è il certificato della CA 'luci.local' per il listener sulla porta 9001. Come per il listener sulla porta 8883, è stata creata una chiave per il server (server.key), una richiesta (server.csr) per ottenere la firma da parte della CA 'luci.local' e il certificato del server (server.crt) firmato dalla CA 'luci.local'. 


Il Mosquitto che gira sulla BB ha 3 listener:
- uno in ascolto sulla porta 1883 che usa il protocollo MQTT,
- uno in ascolto sulla porta 8883 che usa il protocollo MQTT,
- uno in ascolto sulla porta 9001 che usa il protocollo websockets.

Le porte 8883 e 9001 sono protette con TLS. 
Come per il Mosquitto sul computer, anche per questi due listener sono stati creati dei certificati. 
Poichè il microservizio del BBGPIO si connette senza l'uso di certificati, la porta 1883 è stata lasciata aperta. 


Sia nel mosquitto.conf presente sul computer sia nel mosquitto.conf della BB è specificato un file per l'accesso ristretto ai topic e un file contentente le password degli utenti ammessi.


# 5. Descrizione dell'implementazione
## 5.1. UML delle classi implementate

- Webserver 

Responsabile: Alfredo Chissotti

![Webserver](./UML/webserver.jpg)

Il Webserver è concorrente ed espone delle API REST su HTTPS.
Queste API sono richiamate dalla Webapp per ottenere i dati necessari per la visualizzazione delle pagine; in particolare restituiscono le pagine HTML, le immagini, i file CSS e JavaScript.
La classe Helper permette di inviare le risposte HTTPS alla Webapp e di fare controlli, evitando la ripetizione del codice nelle varie classi.



- Domain Manager

Responsabile: Alessandro Carbonelli

![Domain Manager](./UML/domainManager.jpg)

Il microservizio si compone di due packages.
Il package 'code' contiene la logica del microservizio, mentre il package 'db' contiene la classe DBC per l'accesso al database tramite query SQL avvalendosi della libreria JDBC. La persistenza del sistema è affidata ad un DB SQLite locale.

La classe principale del DM è Domain.java. Essa funge da server REST concorrente su Https.
Questa classe espone gli URI che vengono richiamati dalla Webapp per avviare, installare, creare ed eliminare i domini, oltre a fornire servizi per la corretta visualizazione degli elementi in app.
Prima di operare da server esso effettua una chiamata al nostro repository GitLab per popolare la tabella dei moduli del DB.

In qualità di server si avvale di un threadPoolExecutor e per ogni nuova chiamata istanzia un nuovo Thread per servirla.
A seconda dell'URI richiamato viene istanziato un handler diverso, che verifica il token JWT grazie alla classe TokenHandler.

Per verificare che l'utente sia autenticato, viene effettuata una chiamata all'URI della nostra istanza di Keycloak per ottenere la chiave pubblica. Dopo di ciò la firma del JWT che era stato precedentemente inviato nell'header della richiesta della Webapp viene decifrata con la chiave pubblica e viene confrontato il risultato con l'hash(header.body) del JWT. Se il risultato è uguale allora l'utente è verificato. Il metodo che si occupa di ciò è verificaToken che si avvale dell'oggetto di tipo Verifier.

Confermata l'autenticazione dell'utente, è possibile ottenerne il nome utente e distinguere i domini di cui è utente comune o amministratore. A questo punto, a seconda dei servizi richiesti dalla Webapp, si aprono diversi scenari di interazione con il DB ed il CAM (Install, Delete, Start, Stop).
Le chiamate si risolvono una volta che il CAM manda la sua risposta, quindi possiamo considerare la chiamata al CAM incapsulata in termini di tempo all'interno della chiamata da parte della Webapp al DM.



- Luci

Responsabile: Alessandro Carbonelli

![Luci](./UML/luciMicroservizio.jpg)

Il microservizio "Luci" opera in LAN e serve a gestire l'accensione e spegnimento delle lampade nella casa.

La classe principale è Luci.java che, oltre a istanziare il client MQTT, opera su un ArrayList di oggetti di tipo Luce, la struttura dati fondamentale per il funzionamento del microservizio definita dal quartetto input, output, stato e nome. 
Input è la stringa "IN" seguita dal numero che rappresenta quella sorgente in input della BBGPIO.
Output è la string "OUT" seguita dal numero che rappresenta quello specifico output della BBGPIO.
Stato è un boolean che corrisponde all'accensione o spegnimento della lampadina.
Nome è il nome della sala a cui appartiene la luce ed è il suo ID.

Esecutore è la classe che si occupa di attuare l'accensione a tempo delle luci, sfruttando un timer. Necessita di Luci per accedere all'ArrayList<Luce>.

Il sensore di luminosità presente sull'Arduino impedisce ai sensori di movimento di operare se la stanza è troppo illuminata, ovvero se è giorno.

Subscriber legge i comandi dai topic e li gestisce. Per istanziare questa classe, è necessaria un'istanza di Esecutore, Luci ed del sensore di movimento.
Qualora dovesse arrivare un segnale di accensione da parte del sensore di movimento e le luci fossero già accese, Subscriber provvede ad eliminare il vecchio timer dell'Esecutore ed istanziarne uno nuovo.

Questo meccanismo è trasparente al sistema degli interruttori "tradizionali", quindi se un utente vuole accendere una luce tramite interruttore potrà farlo sempre e comunque.
Ad ogni modifica dello stato delle luci corrisponde una modifica del file di stato locale (zona.json), al fine di mantenere le ultime impostazioni in caso di malfunzionamenti o cali di tensione. Questo file viene letto all'avvio del microservizio ed è ciò che garantisce la persistenza.


- Antifurto

Responsabile: Elisa Giglio

![Antifurto](./UML/antifurtoMicroservizio.jpg)

Il microservizio "Antifurto" si compone di 9 classi: Antifurto, Automa, Esecutore, Helper, MyQueue, Pair, Publisher, SubscribeCallback, Timer.

La classe principale è Antifurto. Essa si occupa di far partire il microservizio ed espone alle altre classi:
- un metodo per pubblicare messaggi MQTT, 
- un metodo per sottoscrivere un ulteriore topic,
- un metodo per effettuare l'unsubscribe di un topic.
Sono inoltre presenti getter e setter degli IN e degli OUT in uso dal microservizio. 

L'automa che rappresenta il microservizio Antifurto è scritto nel file automa.json. Basandosi su questo file, la classe Automa gestisce lo stato dell'antifurto. 

La classe generica MyQueue implementa una coda. Il tipo degli oggetti in coda verrà specificato nel momento in cui si crea un'istanza della classe. La gestione dell'intero sistema si basa su due code: una (codaMsg) contenente i messaggi MQTT da pubblicare e una (codaVal) contenente i valori (positivi o negativi) da sommare a una variabile cumulativa mantenuta e aggiornata da Esecutore. 
Gli oggetti nella codaMsg sono istanze della classe Pair. Ogni istanza di Pair è caratterizzata dal topic su cui si vuole pubblicare il messaggio e dal testo del messaggio stesso.

La classe Publisher estrae, uno alla volta, gli oggetti presenti nella codaMsg ed effettua le pubblicazioni su MQTT. 

La classe Esecutore gestisce effettivamente il sistema di antifurto. Ha una variabile cumulativa che viene incrementata e decrementata (solo in caso di antifurto impostato) in base ai valori che si estraggono, uno per volta, dalla codaVal. Questa variabile è confrontata continuamente con il valore soglia: se il suo valore supera la soglia, scatta l'allarme; viceversa, se il suo valore scende sotto la soglia, l'allarme smette di suonare. E' inoltre presente un tempo massimo di allarme: l'allarme non suonerà mai per un periodo superiore a questo tempo. 

In caso di antifurto impostato la classe Timer aggiunge periodicamente un valore negativo alla coda dei valori. Così facendo, il valore della variabile cumulativa della classe Esecutore verrà man mano decrementato a meno di rilevazioni di movimento da parte dei sensori. Questo permette, assime al tempo massimo di allarme, di evitare che l'allarme suoni fino a quando l'utente non disattiva l'antifurto. 

La classe SubscribeCallback gestisce la perdita di connessione con il broker (tentando la riconnessione) e l'arrivo dei messaggi MQTT (in base al messaggio arrivato e al topic su cui è stato pubblicato, viene chiamato un opportuno metodo di questa classe). 

La classe Helper espone metodi per la scrittura e la lettura di file. 


- Scenari

Responsabile: Alfredo Chissotti

![Scenari](./UML/scenariMicroservizio.jpg)

Il microservizio "Scenari" contiene 4 classi fondamentali: Scenari, Automa, SubscribeCallback ed Esecutore.

Scenari è la classe principale: fa partire il microservizio ed espone alle altre classi il metodo per l'invio dei messaggi MQTT, il path delle cartelle di configurazione e di risorse (nella quale si trova la cartella in cui sono salvati gli scenari) e gli IN e OUT attualmente in uso sulla BBGPIO.

L'automa gestisce lo stato in cui si trova il microservizio, che può essere uno dei seguenti: spento, in fase di apprendimento o in fase di esecuzione. Questa classe espone i getter e setter dello stato, permettendo alle altre classi di sapere in qualunque momento quali sono le operazioni possibili. Per salvaguardare la persistenza, l'automa salva lo stato in cui si trova in un file di testo.

SubscribeCallback è la classe che gestisce la ricezione dei messaggi MQTT. Quando riceve un messaggio, lo elabora con un metodo ad hoc che permette di gestirlo autonomamente, anche grazie ai metodi esposti dalle altre classi.

Esecutore è la classe che gestisce l'esecuzione degli scenari a partire dai file presenti nella cartella di risorse. Consiste di un ciclo infinito nel quale si leggono tutti gli scenari salvati e (nel caso in cui l'automa sia nella fase di esecuzione) si fa partire il primo utile, ovvero il primo scenario che ha un orario di esecuzione futuro. In caso contrario, il Thread si sospende per evitare computazione inutile.

Sono inoltre presenti 2 classi di utilità: UserChangeSync ed Helper.
La prima permette all'utente di modificare lo scenario attivo dalla Webapp e quindi di avere un vero impatto su quale scenario stia venendo usato.
La seconda espone dei metodi che vengono usati da più classi.


## 5.2. Descrizione della UI
### 5.2.1. Autenticazione
#### Responsabile: Elisa Giglio
![Login](./WebappPics/login.png)

La prima pagina contiente un unico link, non visibile all'utente. Essa serve per ridirigere l'utente alla pagina di login su Keycloak (mostrata in figura).

Il realm 'test00' ha 4 utenti. Le credenziali degli utenti sono le seguenti:
<table>
<th>Username</th>
<th>Password</th>
<tr>
<td>elisa</td>
<td>elisa</td>
</tr>
<tr>
<td>alfredo</td>
<td>alfredo</td>
</tr>
<tr>
<td>alessandro</td>
<td>alessandro</td>
</tr>
<tr>
<td>john</td>
<td>mariorossi</td>
</tr>
</table>

### 5.2.2. Lista di domini
#### Responsabile: Elisa Giglio
![Lista di domini](./WebappPics/paginaDomini.png)

In questa pagina vengono mostrati tutti i domini appartenenti all'utente autenticato.
Nel caso in cui il dominio sia attivo, un click sul suo nome permette all'utente di visualizzare e gestire i corrispondenti servizi.
Se l'utente è amministratore di un particolare dominio, il bottone dello stato è cliccabile e, in fondo alla riga, è presente un cestino. Un click sul bottone di stato consente di avviare / fermare tutti i microservizi di quel dominio; mentre un click sul cestino elimina l'intero dominio. 
Se l'utente non è amministratore di un dominio non potrà agire sul bottone di stato e non vedrà il cestino: potrà solamente accedere al dominio cliccando sul suo nome (se questo è nello stato ON).

In basso a destra è presente un bottone per effettuare il logout dall'intero sistema. 

### 5.2.3. Interazione con il proprio dominio
#### Responsabile: Alfredo Chissotti
Questa è la pagina principale del progetto, in cui si possono vedere tutti i dettagli del dominio installato. In alto si trovano fino a 3 bottoni che rappresentano i microservizi installati nel dominio scelto. Alla loro pressione, mostrano la UI relativa al loro microservizio, con la quale interagire per apportare modifiche.

- Homepage

![Homepage](./WebappPics/homepage.png)

La pagina principale della Webapp permette l'interazione completa con il sistema. Questo significa che il suo utilizzo permette di avere sia una visione real-time di ciò che accade nella LAN, sia di comandare da remoto gli attuatori e i sensori connessi.

All'avvio della Webapp, viene fatta una RPC ad ogni microservizio installato nel dominio per richiedere e mostrare correttamente tutti i dati attualmente disponibili all'utente; inoltre viene richiesto al server se l'utente è in realtà l'amministratore del dominio, per eventualmente mostrare funzionalità aggiuntive.

In alto si trovano fino a 3 bottoni che, una volta premuti, mostrano la pagina corrispondente al loro microservizio. Di seguito vengono spiegate le pagine che vengono mostrate alla pressione dei bottoni.

- Luci

![Luci](./WebappPics/luci.png)

La pagina è formata da una tabella in cui sono mostrati il nome della luce ed il suo stato. La luce può venire accesa/spenta semplicemente cliccando l'interruttore che ne mostra lo stato.

Al fondo dell'elenco delle luci, è presente un pulsante che permette la creazione di una nuova luce. Questo processo richiede all'utente di scegliere il nome della luce, un pulsante libero e un led libero della BBGPIO. Nel momento in cui la Webapp riceve la conferma di creazione dal microservizio, la luce appena creata viene mostrata a schermo.

- Scenari

![Scenari](./WebappPics/scenari.png)

Questa pagina è composta da una tabella e da dei bottoni al fondo di essa. In questa tabella vengono mostrati gli scenari (il nome, la data di creazione e lo stato), mentre i bottoni permettono di attivare la funzione di apprendimento o l'antifurto. Nel caso in cui il visualizzatore sia l'amministratore del dominio, è anche presente un terzo bottone per modificare gli interruttori usati.

In ogni momento è possibile cambiare lo stato attuale in quello di apprendimento, di antifurto o di attesa.

Mentre l'antifurto è spento, tutti gli scenari sono spenti e non è possibile attivarli; viceversa all'attivazione dell'antifurto viene immediatamente attivato uno scenario da parte del microservizio, che può essere cambiato (attivando uno scenario diverso) o disabilitato (spegnendo lo scenario attivo).

- Antifurto

![Antifurto](./WebappPics/antifurto.png)

La pagina presenta una barra di attenzione, una campanella e lo stato dell'antifurto; inoltre nel caso in cui l'utente sia anche amministratore del dominio, è presente un pulsante che permette la configurazione delle porte, l'aggiunta di un sensore di movimento e la modifica della soglia (ovvero del valore oltre il quale far scattare l'allarme).

Dalla Webapp, un utente normale può solamente attivare/disattivare l'antifurto e controllare il valore a cui è arrivato l'antifurto, oltre a sapere se l'allarme è stato attivato.
# 6. Validazione del software
## 6.1. Procedure usate per verificare il corretto funzionamento del sistema
La validazione del software è stata effettuata in modo procedurale sia durante la fase di sviluppo sia alla sua fine.

Durante la fase di sviluppo, ogni modulo è stato testato singolarmente. Questo ha permesso l'individuazione e la correzione di errori nelle varie implementazioni. 

Successivamente è stata testata l'interazione tra i componenti del sistema: si è verificato che ciascun microservizio fornisse tutti i dati necessari agli altri moduli. 

Nella fase finale sono stati effettuati test di sistema simulando un ambiente reale: sono stati testati tutti i microservizi assieme alla Webapp e agli altri componenti dell'architettura. 
