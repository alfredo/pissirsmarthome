#!/bin/bash

cd keycloak-19.0.1 && bash keycloak.bash &
cd cloudAppManager && bash cloudappmanager.bash &
cd domainManager && bash domain.bash &
cd webserver && bash server.bash &

# wait until <Ctrl>+C is pressed without then closing the process
( trap exit SIGINT; read -r -d '' _ </dev/tty )

#chiudo tutto il resto
bash chiudiprogetto.bash
