#!/bin/bash
status="OFF"
running=$(/usr/bin/pgrep -f code.Luci)
if [ -f "/home/debian/CONFIG/luci/STATUS" ]; then status=$(cat /home/debian/CONFIG/luci/STATUS); fi
if [ ! -z "$running" ]; then status="OFF"; fi
if [ $status == "ON" ]; then (/usr/local/jdk1.8.0_321/bin/java -classpath /home/debian/CONFIG/luci/bin:/home/debian/CONFIG/luci/org.eclipse.paho.client.mqttv3_1.2.5.jar:/home/debian/CONFIG/luci/org.json-1.0.0.v201011060100.jar:/home/debian/CONFIG/luci/bcprov-jdk13-167.jar:/home/debian/CONFIG/luci/bcpkix-jdk13-167.jar code.Luci &>/dev/null&); fi 
