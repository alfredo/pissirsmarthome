package code;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

public class Luce {

	private boolean stato;
	private String IN;
	private String OUT;
	private String nome;


	public Luce(String out, String in, boolean st, String name) throws JSONException, IOException {
		setIN(in);
		setOUT(out);
		setNome(name);
		setStato(st);

	}

	public Luce(JSONObject k) throws JSONException, IOException {
    	this(k.getString("output"),k.getString("input"),k.getBoolean("stato"),k.getString("nome"));//, String lmp, lmp
	}

	public boolean getStato() {
		return stato;
	}


	public void setStato(boolean stato) {
		this.stato = stato;
	}


	public String getIN() {
		return IN;
	}


	public void setIN(String iN) {
		IN = iN;
	}


	public String getOUT() {
		return OUT;
	}


	public void setOUT(String oUT) {
		OUT = oUT;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}
}
