package code;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.time.temporal.Temporal;

import org.json.JSONException;
import org.json.JSONObject;

public class Helper {

	public static boolean compareText(String a, String b){
		return a.compareToIgnoreCase(b) == 0;
	}

	public static String readBody(InputStream requestBody) {
		int req;
		StringBuffer sb = new StringBuffer();
		try {
			while((req = requestBody.read()) != -1)
				sb.append(Character.toString((char)req));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	public static boolean checkJSON(String body) {
		if(!body.startsWith("{") || !body.endsWith("}"))
			return false;
		try {
			new JSONObject(body);
			return true;
		} catch (JSONException e) {
			return false;
		}
	}

	public static String leggiFile(String path) throws IOException {
		String line;
		StringBuffer answer = new StringBuffer();
		BufferedReader bufferedReader = null;
		try {
			bufferedReader = new BufferedReader(new FileReader(path));
			while((line = bufferedReader.readLine()) != null) {
				answer.append(line).append("\n");
			}
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} finally {
			if(bufferedReader != null)
				bufferedReader.close();
		}
		return answer.toString();
	}

	public synchronized static void scriviFile(String text, String path) throws IOException {
		BufferedWriter b = new BufferedWriter(new FileWriter(path));
		String[] lines = text.split("\n");
		for(String l : lines) {
			b.write(l);
			b.newLine();
		}
		b.close();
	}

	public synchronized static void scriviFile(JSONObject job, String path) throws IOException {
		scriviFile(convertiJSON(job),path);
	}

	private static String convertiJSON(JSONObject job) throws IOException {
		String[] strings = job.toString().split(",");
		StringBuffer text = new StringBuffer();
		int index = 1;
		for(String s : strings)
			if(strings.length != index) {
				text.append(s).append(",\n");
				index++;
			} else text.append(s);
		return text.toString();
	}

	public static void appendiFile(String text, String path) throws IOException {
		String fileContents = leggiFile(path);
		StringBuffer s = new StringBuffer();
		if(fileContents!=null && !fileContents.trim().equals(""))
			s.append(fileContents).append("\n");
		s.append(text);
		scriviFile(s.toString(),path);
	}

	public static void appendiFile(JSONObject job, String path, boolean splitLines) throws IOException {
		appendiFile(splitLines ? convertiJSON(job) : job.toString() ,path);
	}

	public static void appendiFile(JSONObject job, String path) throws IOException {
		appendiFile(job,path,true);
	}

	public static long timeDifference(Temporal older, Temporal newer) {
//		return Duration.between(older, newer).toSeconds();
		return Duration.between(older, newer).getSeconds();
	}
}
