package code;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.json.JSONException;

public class Esecutore /* extends Thread */ {

	private final Luci luci;
	static Timer timer;
	public Boolean running;
	private static Esecutore istanza = null;//
	static ArrayList<Luce> j = new ArrayList<Luce>();
	// arraylist mi serve per sapere se una luce era accesa manualmente. Se la era
	// non la spengo quando scade il timer

	private Esecutore(Luci luci) throws IOException, JSONException {
		this.luci = luci;
	}

	public static Esecutore istanziatore(Luci l) throws IOException, JSONException {
		if (istanza == null) {
			istanza = new Esecutore(l);
		}
		return istanza;
	}

	class AccendiTutto extends TimerTask {
		// boolean running=false;
		int durata = 5;
		ArrayList<Luce> j;

		public AccendiTutto(ArrayList<Luce> j) {
			this.j = j;
		}

		public void run() {

			if (durata > 0) {
				String req = "{cmd:1}";
				for (Luce k : Luci.luciList) {
					try {
						luci.sendMqttMessage(Luci.getMqttTree("to", "gpio/" + k.getOUT()), req);
					} catch (MqttException e) {
						e.printStackTrace();
					}

				}
				durata--;
			} else {
				String req = "{cmd:0}";
				for (Luce k : Luci.luciList) {
					if (!j.contains(k)) {
						k.setStato(false);
						try {
							luci.sendMqttMessage(Luci.getMqttTree("to", "gpio/" + k.getOUT()), req);
						} catch (MqttException e) {
							e.printStackTrace();
						}
					}
				}
				j.clear();
				timer.cancel();
				timer.purge();
				Subscriber.runningEsec = false;
			}
		}
	}

	public void run(Boolean running) {

		timer = new Timer();
		AccendiTutto a = new AccendiTutto(j);
		this.running = running;
		timer.schedule(a, 0, 1000);
	}

}