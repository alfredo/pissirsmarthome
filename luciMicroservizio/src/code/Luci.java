package code;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.Security;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
//import java.util.Collections;
import java.util.Date;
//import java.util.Iterator;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMDecryptorProvider;
import org.bouncycastle.openssl.PEMEncryptedKeyPair;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.openssl.jcajce.JcePEMDecryptorProviderBuilder;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttTopic;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
//import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Luci {

	public static ArrayList<Luce> luciList;
	public static boolean shouldPrint = false;
	private static String brokerUrl;

	//private JSONObject jsonObject;
	//private final static String DATABASE_PATH = "./Luci.json";

	private static ArrayList<String> topicsSub;
	private String clientId = Long.toString(new Date().getTime()) + "-luci"; // unique client id
	private String mqttDomain;
	private String mqttSubdomain;
	private static String mqttTree;
	private static final String BASE_DIR = "/home/debian/CONFIG/luci/";
	public static final String CONF_FOLDER = BASE_DIR + "CONF/";
	private final static String FILE_CONF = CONF_FOLDER + "conf.json";// "./CONF/conf.json";
	// private final static String FILE_ZONA =
	// CONF_FOLDER+"zona.json";//"./CONF/zona.json"
	final static String FILE_ZONA = BASE_DIR + "CONF/zona.json";// "./CONF/zona.json"

	public MqttClient mqttClient;

	public Luci() throws JSONException, IOException, MqttException {

		JSONObject config = new JSONObject(Helper.leggiFile(FILE_CONF));// dove mi salvo le strutture dal file? cioe'
																		// leggo e poi?
		brokerUrl = config.getString("protocol") + "://" + config.getString("broker") + ":" + config.getInt("port");
		mqttDomain = config.getString("mqttDomain");
		mqttSubdomain = config.getString("mqttSubdomain");
		makeMqttTree();

		topicsSub = new ArrayList<String>();
		topicsSub.add("to/all"); // mandare la mia descrizione json
		topicsSub.add(getMqttTree("rpc/", "luci")); // mandare lo stato attuale

		// inutile aggiungere i topic, perche subbo gia' tutto gpio/#
		config = new JSONObject(Helper.leggiFile(FILE_ZONA));

		topicsSub.add(getMqttTree("from/", "gpio/#"));
		topicsSub.add(getMqttTree("to/", "luci/#"));// {"evento":0,"nome":nome_scenario} per attivare/disattivare uno
		topicsSub.add(getMqttTree("from/", "arduino/AN0"));			// scenario; risposta su from/tree/scenari/attiva con
													// {"nome":nome_scenario}
		// topicsSub.add(getMqttTree("to/","luci/salva"));//per rinominare lo scenario
		// piu recente

		this.mqttClient = new MqttClient(brokerUrl, clientId, new MemoryPersistence());

	}

	private void makeMqttTree() {
		if (mqttTree != null)
			return;
		mqttTree = mqttDomain + "/" + mqttSubdomain + "/";
	}

	public static String getMqttTree(String prefix, String suffix) {
		return (prefix.endsWith("/") ? prefix : prefix + "/") + mqttTree
				+ (suffix.startsWith("/") ? suffix.substring(1) : suffix);
	}

	public static void main(String args[]) throws JSONException, IOException, MqttException {
		if(args.length > 0)
			Luci.shouldPrint = true;
		System.out.println("luci started");
		luciList = new ArrayList<Luce>();
		startSystemo();
	}

	private static void startSystemo() throws JSONException, IOException, MqttException {
		JSONObject config = new JSONObject(Helper.leggiFile(FILE_ZONA));
		String sensM = config.getString("sensM");
		int index = 0;
		while (config.has("lamp" + index)) {
			JSONObject k = config.getJSONObject("lamp" + index);
			String a = k.getString("output");
			String b = k.getString("input");
			boolean c = k.getBoolean("stato");
			String d = k.getString("nome");

			Luce luce = new Luce(a, b, c, d);

			luciList.add(luce);
			index++;
		}

		Luci luci = new Luci();
		Esecutore esec = Esecutore.istanziatore(luci);
		luci.startClient(esec, sensM);
	}

	public void sendMqttMessage(String topic, String msg) throws MqttException {
		final MqttTopic msgTopic = mqttClient.getTopic(topic);
		msgTopic.publish(new MqttMessage(msg.getBytes()));
	}

	public void startClient(Esecutore esec, String sensM) throws MqttException, JSONException {
		String caFilePath = "";
		String clientCrtFilePath = "";
		String clientKeyFilePath = "";

		MqttConnectOptions options = new MqttConnectOptions();
		options.setCleanSession(false);
		if(brokerUrl.contains("luci.local")) {  // devo connettermi al mosquitto della beaglebone
			caFilePath = BASE_DIR + "certificates/beaglebone/caGruppo2.crt";
			clientCrtFilePath = BASE_DIR + "certificates/beaglebone/clientGruppo2.crt";
			clientKeyFilePath = BASE_DIR + "certificates/beaglebone/clientGruppo2.key";

			options.setUserName("gruppo2");
			options.setPassword("funziona".toCharArray());
		}
		else {
			System.out.println("Unknown broken url " + brokerUrl);
			System.exit(1);
		}

		options.setMqttVersion(MqttConnectOptions.MQTT_VERSION_3_1);

		SSLSocketFactory socketFactory;
		try {
			socketFactory = getSocketFactory(caFilePath, clientCrtFilePath, clientKeyFilePath, "");
			options.setSocketFactory(socketFactory);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Subscriber subscriber = new Subscriber(this, esec, sensM);
		mqttClient.setCallback(subscriber);

		mqttClient.connect(options);

		for (String t : topicsSub)
			mqttClient.subscribe(t);

		// send Scenari the OUTs for every light available
		subscriber.sendOutsToScenari();
	}


	private static SSLSocketFactory getSocketFactory(final String caCrtFile,
			final String crtFile, final String keyFile, final String password)
			throws Exception {
		Security.addProvider(new BouncyCastleProvider());

		// load CA certificate
		X509Certificate caCert = null;

		FileInputStream fis = new FileInputStream(caCrtFile);
		BufferedInputStream bis = new BufferedInputStream(fis);
		CertificateFactory cf = CertificateFactory.getInstance("X.509");

		while (bis.available() > 0) {
			caCert = (X509Certificate) cf.generateCertificate(bis);
		}

		// load client certificate
		bis = new BufferedInputStream(new FileInputStream(crtFile));
		X509Certificate cert = null;
		while (bis.available() > 0) {
			cert = (X509Certificate) cf.generateCertificate(bis);
		}

		// load client private key
		PEMParser pemParser = new PEMParser(new FileReader(keyFile));
		Object object = pemParser.readObject();
		PEMDecryptorProvider decProv = new JcePEMDecryptorProviderBuilder()
				.build(password.toCharArray());
		JcaPEMKeyConverter converter = new JcaPEMKeyConverter()
				.setProvider("BC");
		KeyPair key;
		if (object instanceof PEMEncryptedKeyPair) {
			//System.out.println("Encrypted key - we will use provided password");
			key = converter.getKeyPair(((PEMEncryptedKeyPair) object)
					.decryptKeyPair(decProv));
		} else {
			//System.out.println("Unencrypted key - no password needed");
			key = converter.getKeyPair((PEMKeyPair) object);
		}
		pemParser.close();

		// CA certificate is used to authenticate server
		KeyStore caKs = KeyStore.getInstance(KeyStore.getDefaultType());
		caKs.load(null, null);
		caKs.setCertificateEntry("ca-certificate", caCert);
		TrustManagerFactory tmf = TrustManagerFactory.getInstance("X509");
		tmf.init(caKs);

		// client key and certificates are sent to server so it can authenticate
		// us
		KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
		ks.load(null, null);
		ks.setCertificateEntry("certificate", cert);
		ks.setKeyEntry("private-key", key.getPrivate(), password.toCharArray(),
				new java.security.cert.Certificate[] { cert });
		KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory
				.getDefaultAlgorithm());
		kmf.init(ks, password.toCharArray());

		// finally, create SSL socket factory
		SSLContext context = SSLContext.getInstance("TLSv1.2");
		context.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

		return context.getSocketFactory();
	}
}
