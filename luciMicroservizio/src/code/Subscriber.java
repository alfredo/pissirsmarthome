package code;

import java.io.IOException;
import java.util.Date;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

//import scenari.Automa;
//import scenari.Scenari;

public class Subscriber implements MqttCallback {

	private Luci luci;
	private Esecutore esec;
	static Boolean runningEsec;
	public String sensM;
	Boolean giorno=false;

	public Subscriber(Luci luci, Esecutore esec, String sensm) {
		this.luci = luci;
		this.esec = esec;
		this.sensM = sensm;
		Subscriber.runningEsec = false;
	}

	@Override
	public void connectionLost(Throwable arg0) {
		final Date d = new Date();
		Date d2 = new Date();
		long time = Math.abs(d2.getTime()-d.getTime());
		boolean retry = true;
		while (retry && (time<600000)) {
			try {
				luci.startClient(esec,sensM);
				retry = false;
			} catch (MqttException e) {
				d2 = new Date();
				time = Math.abs(d2.getTime()-d.getTime());
			} catch (JSONException j) {
				retry = false;
			}
		}
		if(time>=600000) {
			System.exit(1);
		}
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken arg0) {}

	@Override
	public void messageArrived(String topic, MqttMessage message) throws MqttException, JSONException, IOException {

		if (topic.equals(Luci.getMqttTree("rpc/", "luci"))) {
			sendFile(Luci.getMqttTree("from/", "luci"));
			return;
		}

		if (topic.equals("to/all")) {
			sendStatus(Luci.getMqttTree("from/", "luci"));
			return;
		}

		JSONObject msgJson = new JSONObject(message.toString());

		// la beaglebone risponde sutopic {"event":1} se e' acceso, 0 altrimenti

		if (topic.equals(Luci.getMqttTree("to", "luci/new"))) {

			JSONObject config = new JSONObject(Helper.leggiFile(Luci.FILE_ZONA));

			String nome = msgJson.getString("nome");
			for (Luce k : Luci.luciList) {
				if (k.getNome().equalsIgnoreCase(nome)) {
					return;
				}
			}
			int k = Luci.luciList.size();
			config.put("lamp" + k, msgJson);
			Luci.luciList.add(new Luce(msgJson));// ,"lamp"+k
			Helper.scriviFile(config, Luci.FILE_ZONA);

			luci.sendMqttMessage(Luci.getMqttTree("from", "luci/new"), msgJson.toString());

			return;
		}

		if (topic.equals(Luci.getMqttTree("to", "luci/sensore"))) {//FIXME this isn't currently used
			// salvati il sensore di movimento

			sensM = msgJson.getString("sensM");
			// devo aggiornare in file
			JSONObject config = new JSONObject(Helper.leggiFile(Luci.FILE_ZONA));
			config.put("sensM", sensM);
			Helper.scriviFile(config, Luci.FILE_ZONA);

			luci.sendMqttMessage(Luci.getMqttTree("from", "luci/sensore"), msgJson.toString());
			return;

		} // gruppo2/luci/luci/sensore

		// messaggio da parte di scenari per inviargli tutte le luci
		if(topic.equals(Luci.getMqttTree("to", "luci/outs"))) {
			sendOutsToScenari();
			return;
		}

		// sensM tempo
		if (topic.equals(Luci.getMqttTree("from", "gpio/" + sensM))) {
			if(giorno)return;

			if (msgJson.has("event") && msgJson.getInt("event") == 1) {
				if (runningEsec) {
					Esecutore.timer.cancel();
					Esecutore.timer.purge();
				} else {
					for (Luce k : Luci.luciList) {
						if (k.getStato())
							Esecutore.j.add(k);
					}
				}
				runningEsec = true;
				esec.run(runningEsec);
//
			}
			return;
		}

		// controllo i topic su cui mi arrivano gli on/off
		if (topic.startsWith(Luci.getMqttTree("from", "gpio/IN"))) {
			// prendo l'ultimo pezzo del topic, diventa il mio nome

			String pezzo = topic.substring(23);// seleziono solo il nome della lamp
			for (int i = 0; i < Luci.luciList.size(); i++) {
				if (Luci.luciList.get(i).getIN().equalsIgnoreCase(pezzo) && msgJson.has("event")
						&& msgJson.getInt("event") == 1) {// non e' k.getIN()?

					String req = Luci.luciList.get(i).getStato() ? "{cmd:0}" : "{cmd:1}";
					luci.sendMqttMessage(Luci.getMqttTree("to", "gpio/" + Luci.luciList.get(i).getOUT()), req);
				}
			}
			return;
		} // gruppo2/luci/luci/sensore

		if (topic.startsWith(Luci.getMqttTree("from", "gpio/OUT"))) {
			if (msgJson.has("status"))
				return;
			String pezzo = topic.substring(23);// seleziono solo il nome della lamp

			for (int i = 0; i < Luci.luciList.size(); i++) {
				if (Luci.luciList.get(i).getOUT().equalsIgnoreCase(pezzo) && msgJson.has("event")) {
					// && msgJson.getInt("event")==1)

					boolean stato = msgJson.getInt("event") == 1;

					Luci.luciList.get(i).setStato(stato);// (!Luci.luciList.get(i).getStato());

					JSONObject config = new JSONObject(Helper.leggiFile(Luci.FILE_ZONA));

					JSONObject lux = new JSONObject();
					lux.put("nome", Luci.luciList.get(i).getNome());
					lux.put("stato", Luci.luciList.get(i).getStato());
					lux.put("input", Luci.luciList.get(i).getIN());
					lux.put("output", Luci.luciList.get(i).getOUT());

					config.put("lamp" + i, lux);

					Helper.scriviFile(config, Luci.FILE_ZONA);
					// mando conferma
					lux.remove("input");
					lux.remove("nome");
					luci.sendMqttMessage(Luci.getMqttTree("from", "luci/luce"), lux.toString());

				}
			}
			return;
		}

		if (topic.startsWith(Luci.getMqttTree("from", "arduino/AN0"))) {
			if (msgJson.has("event")){
//
//				if(msgJson.getInt("event")<200)giorno=false;
//				else giorno=true;

				giorno=(msgJson.getInt("event")>=200);
			}
		return;
		}


		System.err.println("[Luci] Impossibile: "+topic);
	}

	private void sendFile(String topic) throws MqttException, JSONException, IOException {
		JSONObject j = new JSONObject(Helper.leggiFile(Luci.FILE_ZONA));

		luci.sendMqttMessage(topic, j.toString());
	}

	private void sendStatus(String topic) throws MqttException, JSONException, IOException {
		JSONObject j = new JSONObject(Helper.leggiFile(Luci.FILE_ZONA));
		j.remove("sensM");

		luci.sendMqttMessage(topic, j.toString());
	}

	public void sendOutsToScenari() throws JSONException, MqttException{
		JSONObject response = new JSONObject();
		JSONArray j = new JSONArray();
		for (Luce k : Luci.luciList) {
			j.put(k.getOUT());
		}
		response.put("luci", j);
		luci.sendMqttMessage(Luci.getMqttTree("to","scenari/luci"), response.toString());
	}

}