#!/bin/bash
status="OFF"
running=$(/usr/bin/pgrep -f code.Antifurto)
if [ -f "/home/debian/CONFIG/antifurto/STATUS" ]; then status=$(cat /home/debian/CONFIG/antifurto/STATUS); fi
if [ ! -z "$running" ]; then status="OFF"; fi
if [ $status == "ON" ]; then (/usr/local/jdk1.8.0_321/bin/java -classpath /home/debian/CONFIG/antifurto/bin:/home/debian/CONFIG/antifurto/org.eclipse.paho.client.mqttv3_1.2.5.jar:/home/debian/CONFIG/antifurto/org.json-1.0.0.v201011060100.jar:/home/debian/CONFIG/antifurto/bcprov-jdk13-167.jar:/home/debian/CONFIG/antifurto/bcpkix-jdk13-167.jar code.Antifurto &>/dev/null&); fi 
