package code;
import org.eclipse.paho.client.mqttv3.MqttException;


public class Publisher extends Thread {

	private MyQueue<Pair> coda;
	private Antifurto client;
	
	public Publisher(MyQueue<Pair> coda, Antifurto client) {
		this.coda = coda;
		this.client = client;
	}
	
	public void aggiungiComando(String topic, String msg) {
		Pair p = new Pair(topic, msg);  
		coda.send(p);
	}
	
	
	public void run() {
		while(true) {
			Pair p = coda.receive();
			try {
				client.publishMethod(p.getTopic(), p.getMsg());
			} catch (MqttException e) {
				e.printStackTrace();
			}
		}
	}
}
