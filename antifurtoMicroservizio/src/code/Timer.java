package code;
public class Timer extends Thread {

	private int delay;
	private int deltaK; // e' un valore negativo
	private Esecutore esec;
	private Automa automa;

	public Timer(int delay, int deltaK, Esecutore esec, Automa automa) {
		this.delay = delay;
		this.deltaK = deltaK;
		this.esec = esec;
		this.automa = automa;
	}

	public void run() {
		while(true) {
			while(automa.antifurtoAttivo()) { // finche' l'antifurto e' acceso
				esec.aggiungiVal(deltaK);

				try {
					Thread.sleep(delay);
				} catch(InterruptedException e) {
					System.out.println(e);
				}
			}
		}
	}

}
