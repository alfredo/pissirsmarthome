package code;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

public class Helper {
	
	public static synchronized void scriviFile(JSONObject json, String path) throws IOException {
		BufferedWriter b = null;
		try {
			b = new BufferedWriter(new FileWriter(path));
			b.write(json.toString()); 
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} finally {
			if(b != null)
				b.close();
		}
	}
	
	public static synchronized String leggiFile(String path) throws IOException, JSONException {
		String line;
		StringBuffer answer = new StringBuffer();
		BufferedReader bufferedReader = null;
		try {
			bufferedReader = new BufferedReader(new FileReader(path));
			while((line = bufferedReader.readLine()) != null) {
				answer.append(line).append("\n");
			}
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} finally {
			if(bufferedReader != null)
				bufferedReader.close();
		}
		
		try {
			new JSONObject(answer.toString());
		} catch (JSONException e) {
			if(path.contains("stato.json")) {
				return new JSONObject().put("stato", 0).put("soglia", 30).put("valore", 0).put("tempoAllarme", "2022-11-20T18:55:59.768946").toString();
			}
		}
		
		return answer.toString();
	}
	
}
