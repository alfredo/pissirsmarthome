package code;
import java.util.ArrayList;

public class MyQueue<T> {

	private ArrayList<T> queue;
	
	public MyQueue() {
		this.queue = new ArrayList<T>();
	}
	
	public boolean isEmpty() {
		return queue.isEmpty();
	}
	
	public synchronized void send(T n) {
		queue.add(n);
		notifyAll();
	}
	
	public synchronized T receive() {
		while(isEmpty()) {
			try {
				wait();
			} catch(InterruptedException ex) {
				System.out.println("Interrupted exception");
			}
		}
		return queue.remove(0);
	}
	
	public synchronized void removeAll() {
		queue.clear();
	}
}
