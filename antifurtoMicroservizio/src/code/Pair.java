package code;
import org.json.JSONException;
import org.json.JSONObject;

public class Pair {

	private String topic;
	private String msg;
	
	
	public Pair(String topic, String msg) {
		this.topic = topic;
		this.msg = msg;
	}
	
	public String getTopic() {
		return topic;
	}
	
	public String getMsg() {
		return msg;
	}
	
	public JSONObject toJsonObject() throws JSONException {
		JSONObject js = new JSONObject();
		js.put("topic", topic);
		js.put("msg", msg); 
		return js;
	}
	
	public String toString() {
		try {
			return toJsonObject().toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
}
