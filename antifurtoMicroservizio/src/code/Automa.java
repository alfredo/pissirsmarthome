package code;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

public class Automa {
	private JSONObject jsonObject; //tabella che descrive l'automa
	private Hashtable<String,Integer> deltaSensori; // ad ogni sensore di movimento e' associato il valore di delta
	private int statoAttuale;
	private String statoInterruttore;
	private String statoSuono;
	private static final String FILE_AUTOMA = Antifurto.PATH_BB + "/res/automa.json";
	public static final String FILE_STATO = Antifurto.PATH_BB + "/res/stato.json";
	public static final String FILE_DELTA_SENSORI = Antifurto.PATH_BB + "/res/CONF/deltaSensoriMovimento.json";
	
	
	public Automa() throws JSONException, IOException {
		this.jsonObject = new JSONObject(Helper.leggiFile(FILE_AUTOMA));
		setStati(false);
		riempiHashTable();
		
	}
	
	
	private synchronized void setStati(boolean write) throws JSONException, IOException {
		JSONObject statoJson = new JSONObject(Helper.leggiFile(FILE_STATO)); 
		if(write) {
			// devo aggiornare lo stato nel FILE_STATO
			Helper.scriviFile(statoJson.put("stato",this.statoAttuale), FILE_STATO);
		}
		else {
			// devo capire in quale stato mi trovo e impostare di conseguenza lo statoInterruttore e lo statoSuono
			this.statoAttuale = statoJson.getInt("stato");
		}
		aggiornaInterruttoreESuono();
	}
	
	
	private void riempiHashTable() throws JSONException, IOException {
		this.deltaSensori = new Hashtable<String, Integer>();
		JSONObject jsObj = new JSONObject(Helper.leggiFile(FILE_DELTA_SENSORI));
		@SuppressWarnings("unchecked")
		Iterator<String> keys = jsObj.keys();
		while(keys.hasNext()) {
			String key = keys.next();
			int value = jsObj.getInt(key);
			deltaSensori.put(key, value);
		}
	}
	
	public void addDeltaSensori(String nomeSensore, int delta) {
		deltaSensori.put(nomeSensore, delta);
	}
	

	
	public int getDelta(String nomeSensore) {
		return deltaSensori.get(nomeSensore);
	}
	
	
	// e' cambiato lo stato dell'interruttore, per cui cerco nel file json in quale nuovo stato devo andare
	private synchronized void calcolaNuovoStatoMsgFromInterruttore(String newStatoInterruttore) throws JSONException, IOException {
		@SuppressWarnings("unchecked")
		Iterator<String> keys = jsonObject.keys();
		while(keys.hasNext()) {
			String key = keys.next();
			JSONObject riga = jsonObject.getJSONObject(key);
			if(riga.getInt("stato")==statoAttuale) {
				this.statoAttuale = riga.getInt("cmd"+newStatoInterruttore);
				setStati(true); //scrivo nel file il nuovo stato in cui mi trovo
			}
		}
	}
	//in base allo stato attuale dell'antifurto, calcolo lo stato dell'interruttore e del suono
	public synchronized void aggiornaInterruttoreESuono() throws JSONException {
		@SuppressWarnings("unchecked")
		Iterator<String> keys = jsonObject.keys();
		while(keys.hasNext()) {
			String key = keys.next();
			JSONObject riga = jsonObject.getJSONObject(key);
			if(riga.getInt("stato")==statoAttuale) {
				this.statoInterruttore = riga.getString("out0");
				this.statoSuono = riga.getString("out1");
			}
		}
	}
	
	// e' cambiato lo stato del suono, per cui cerco nel file json in quale nuovo stato devo andare
	private synchronized void calcolaNuovoStatoMsgFromSuono(String newStatoSuono) throws JSONException, IOException {
		String jsonKey = "";
		if(newStatoSuono.equals("on"))
			jsonKey = "eventValMaggioreSoglia";
		else
			jsonKey = "eventValMinoreSoglia";
		@SuppressWarnings("unchecked")
		Iterator<String> keys = jsonObject.keys();
		while(keys.hasNext()) {
			String key = keys.next();
			JSONObject riga = jsonObject.getJSONObject(key);
			if(riga.getInt("stato")==statoAttuale) {
				this.statoAttuale = riga.getInt(jsonKey);
				setStati(true); //scrivo nel file il nuovo stato in cui mi trovo
			}
		}
	}
	
	
	public synchronized void changeStatoInterruttore() throws JSONException, IOException {
		if(statoInterruttore.equals("on"))
			calcolaNuovoStatoMsgFromInterruttore("off");
		else 
			calcolaNuovoStatoMsgFromInterruttore("on");
	}
	
	public synchronized void aggiornaStatoSuono(String newStatoSuono) throws JSONException, IOException {
		calcolaNuovoStatoMsgFromSuono(newStatoSuono);
	}
	
	
	public synchronized int getStatoAutoma() {
		return statoAttuale;
	}
	
	public synchronized void setStatoAutoma(int newStato) {
		statoAttuale = newStato;
	}
	
	
	public synchronized String getStatoInterruttore() {
		return statoInterruttore;
	}
	
	
	public synchronized String getStatoSuono() {
		return statoSuono;
	}
	
	public synchronized boolean getStatoSuonoTrueFalse() {
		return statoSuono.equals("on");
	}
	
	public synchronized boolean getStatoInterrutoreTrueFalse() {
		return statoInterruttore.equals("on");
	}
	
	
	// Ritorna true se l'antifurto e' attivo
	public synchronized boolean antifurtoAttivo() {
		if(statoInterruttore.equals("on"))
			return true;
		return false;
	}
	
	// Ritorna true se l'antifurto sta suonando
	public synchronized boolean allarme() {
		if(statoSuono.equals("on"))
			return true;
		return false;
	}
	
	
	// con input la stringa "0" restituisce "off"
	// con input la stringa "1" restituisce "on"
	public String converter(int num) {
		if(num==0)
			return "off";
		return "on";
	}
}
